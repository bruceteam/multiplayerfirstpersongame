// MFPS by Oleg Sutugin. All rights reserved


#include "PickUp/MFPSHealthPickUp.h"
#include "Components/MFPSHealthComponent.h"
#include "MFPSUtils.h"

DEFINE_LOG_CATEGORY_STATIC(LogHealthPickUp, All, All);

bool AMFPSHealthPickUp::GivePickUpTo(APawn* Pawn)
{
	const auto HealthComponent = MFPSUtils::GetMFPSPlayerComponent<UMFPSHealthComponent>(Pawn);
	if (!HealthComponent || HealthComponent->bIsDead()) return false;


	return HealthComponent->TryToAddHealth(HealValue);
}
