// MFPS by Oleg Sutugin. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MFPSHealthBarWidget.generated.h"

class UProgressBar;

UCLASS()
class MFPS_API UMFPSHealthBarWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	void SetHealthPercent(float Percent);
	
	UPROPERTY(meta = (BindWidget))
		UProgressBar* HealthProgressBar;
protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI")
		float PercentVisibilityTreshold = 0.8f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI")
		float PercentGoodColorTreshold = 0.6f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI")
		float PercentBadColorTreshold = 0.3f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI")
		FLinearColor GoodColor = FLinearColor::Green;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI")
		FLinearColor MiddleColor = FLinearColor::Yellow;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI")
		FLinearColor BadColor = FLinearColor::Red;

};
