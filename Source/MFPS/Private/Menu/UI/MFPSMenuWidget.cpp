// MFPS by Oleg Sutugin. All rights reserved


#include "Menu/UI/MFPSMenuWidget.h"
#include "Menu/MFPSMenuGameModeBase.h"
#include "Components/Button.h"
#include "Kismet/KismetSystemLibrary.h"
#include "MFPSGameInstance.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"

DEFINE_LOG_CATEGORY_STATIC(LogMenuWidget, All, All);

void UMFPSMenuWidget::NativeOnInitialized()
{
	if (StartGameButton)
	{
		StartGameButton->OnClicked.AddDynamic(this, &UMFPSMenuWidget::OnStartGame);
	}

	if (QuitGameButton)
	{
		QuitGameButton->OnClicked.AddDynamic(this, &UMFPSMenuWidget::OnQuitGame);
	}

	Super::NativeOnInitialized();
}

void UMFPSMenuWidget::OnStartGame()
{
	//const FName StartUpLevelName = "GrayBox";

	if (!GetWorld()) return;

	UGameplayStatics::PlaySound2D(GetWorld(), StartGameSound);

	const auto MFPSGameInstance = GetWorld()->GetGameInstance<UMFPSGameInstance>();
	if (!MFPSGameInstance) return;

	if (MFPSGameInstance->GetStartupLevelName().IsNone())
	{
		UE_LOG(LogMenuWidget, Error, TEXT("Level Name Is none"));
		return;
	}

	GetWorld()->ServerTravel(MFPSGameInstance->GetStartupLevelName().ToString(), false);
	//UGameplayStatics::OpenLevel(this, StartUpLevelName);

	/*const auto GM = Cast<AMFPSMenuGameModeBase>(GetWorld()->GetAuthGameMode());
	if (!GM) return;

	GM->GetWorld()->ServerTravel(StartUpLevelName.ToString(), false);*/
	//UGameplayStatics::OpenLevel(this, StartUpLevelName);
}

void UMFPSMenuWidget::OnQuitGame()
{
	//UKismetSystemLibrary::QuitGame(this, GetOwningPlayer(), EQuitPreference::Quit, true);
	GetOwningPlayer()->ConsoleCommand("Exit"); 
}
