// MFPS by Oleg Sutugin. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "PickUp/MFPSBasePickUp.h"
#include "MFPSHealthPickUp.generated.h"


UCLASS()
class MFPS_API AMFPSHealthPickUp : public AMFPSBasePickUp
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PickUp", meta = (ClampMin = "1", ClampMax = "100"))
		float HealValue = 50.0f;

	virtual bool GivePickUpTo(APawn* Pawn) override;
};
