// MFPS by Oleg Sutugin. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "MFPSRespawnComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MFPS_API UMFPSRespawnComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UMFPSRespawnComponent();

	void Respawn(int32 RespawnTime);

	//for widget
	int32 ReturnRespawnCountDown() const { return RespawnCountDown; }
private:
	FTimerHandle RespawnTimerHandle;
	UPROPERTY(Replicated)
	int32 RespawnCountDown = 0;

	
	void RespawnTimerUpdate();

};
