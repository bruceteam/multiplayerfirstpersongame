// MFPS by Oleg Sutugin. All rights reserved


#include "Components/MFPSWeaponFXComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Sound/SoundCue.h"
#include "Components/DecalComponent.h"


UMFPSWeaponFXComponent::UMFPSWeaponFXComponent()
{
	PrimaryComponentTick.bCanEverTick = false;


}

void UMFPSWeaponFXComponent::S_PlayImpactFX_Implementation(const FHitResult& Hit)
{
	M_PlayImpactFX(Hit);
}

void UMFPSWeaponFXComponent::M_PlayImpactFX_Implementation(const FHitResult& Hit)
{
	auto ImpactData = DefaultImpactData;

	if (Hit.PhysMaterial.IsValid())
	{
		const auto PhysMat = Hit.PhysMaterial.Get();
		if (ImpactDataMap.Contains(PhysMat))
		{
			ImpactData = ImpactDataMap[PhysMat];
		}
	}

	UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(),//
		ImpactData.NiagaraEffect,			     			  //
		Hit.ImpactPoint,									  //
		Hit.ImpactNormal.Rotation());

	auto DecalComponent = UGameplayStatics::SpawnDecalAtLocation(GetWorld(),//
		ImpactData.DecalData.Material,										//
		ImpactData.DecalData.DecalSize,										//
		Hit.ImpactPoint,													//
		Hit.ImpactNormal.Rotation());

	if (DecalComponent)
	{
		DecalComponent->SetFadeOut(ImpactData.DecalData.LifeTime, ImpactData.DecalData.FadeOutTime);
		DecalComponent->SetFadeScreenSize(0.f);
	}

	UGameplayStatics::PlaySoundAtLocation(GetWorld(), ImpactData.ImpactSound, Hit.ImpactPoint);
}

