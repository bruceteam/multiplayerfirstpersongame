// MFPS by Oleg Sutugin. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MFPSProjectile.generated.h"

class USphereComponent;
class UProjectileMovementComponent;
class UMFPSWeaponFXComponent;

UCLASS()
class MFPS_API AMFPSProjectile : public AActor
{
	GENERATED_BODY()

public:

	AMFPSProjectile();

	void SetShotDirection(const FVector& Direction) { ShotDirection = Direction; }
protected:

	virtual void BeginPlay() override;

	UPROPERTY(VisibleDefaultsOnly, Category = "Weapon")
		USphereComponent* CollisionComponent;

	UPROPERTY(BlueprintReadOnly, Category = "Weapon")
		UProjectileMovementComponent* MovementComponent;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
		float DamageByHit = 5.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
		UMFPSWeaponFXComponent* WeaponFXComponent;


private:
	FVector ShotDirection;

	UFUNCTION()
		void OnProjectileHit(UPrimitiveComponent* HitComponent, //
			AActor* OtherActor,                                 //
			UPrimitiveComponent* OtherComp,                     //
			FVector NormalImpulse,                              //
			const FHitResult& Hit);

	AController* GetController() const;
};
