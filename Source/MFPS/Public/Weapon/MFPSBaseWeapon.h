// MFPS by Oleg Sutugin. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MFPSTypes.h"
#include "MFPSBaseWeapon.generated.h"



class USkeletalMeshComponent;
class UNiagaraSystem;
class UNiagaraComponent;
class AMFPSDummyWeapon;
class USoundCue;

UCLASS()
class MFPS_API AMFPSBaseWeapon : public AActor
{
	GENERATED_BODY()

public:

	AMFPSBaseWeapon();

	FOnEmptyClipSignature OnEmptyClip;

	FWeaponUIData GetWeaponUIData() const { return UIData; }

	UFUNCTION(Server, Reliable)
	virtual void StartFire();
	UFUNCTION(Server, Reliable)
	virtual void StopFire();
	
	virtual void MakeShot();

	void ChangeClip();
	bool CanReload() const;
	FAmmoData GetAmmoData() const { return CurrentAmmo; }

	bool TryToAddAmmo(int32 ClipsValue);

protected:

	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Component")
		USkeletalMeshComponent* WeaponMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Component")
		FName MuzzleSocketName = "MuzzleSocket";

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Component")
		float TraceMaxDistance = 2500.0f;

	UPROPERTY(Replicated,EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
		FAmmoData DefaultAmmo{15, 10, false};

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
		FWeaponUIData UIData;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
		UNiagaraSystem* MuzzleFX;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sound")
		USoundCue* FireSound;

	//refactoring
	APlayerController* GetPlayerController() const;
	bool GetPlayerViewPount(FVector& ViewLocation, FRotator& ViewRotation) const;
	FVector GetMuzzleWorldLocation() const;
	virtual bool GetTraceData(FVector& TraceStart, FVector& TraceEnd) const;
	void MakeHit(FHitResult& HitResult, const FVector& TraceStart, FVector& TraceEnd);


	void DecreaseAmmo();

	bool IsAmmoEmpty() const;
	bool IsClipEmpty() const;
	bool IsAmmoFull() const;

	void LogAmmo();

	UNiagaraComponent* MuzzleFXComponent;
	
	UFUNCTION(Client, Reliable)
	void SpawnMuzzleFX();

private:
	UPROPERTY(Replicated)
	FAmmoData CurrentAmmo;
};
