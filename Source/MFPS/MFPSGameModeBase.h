// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MFPSTypes.h"
#include "MFPSGameModeBase.generated.h"


class AMFPSPlayerController;

UCLASS()
class MFPS_API AMFPSGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	AMFPSGameModeBase();

	void StartPlayWithDelay();
	virtual void StartPlay() override;

	void Killed(AController* KillerController, AController* VictimController);
	
	UFUNCTION()
	void SetRoundCountDown(int32 Value);

	void RespawnRequest(AController* Controller);

	FOnGameOverSignature OnGameOver;

protected:
	UPROPERTY(EditDefaultsOnly, Category = "Game")
		TSubclassOf<AMFPSPlayerController> ControllerClass;

	UPROPERTY(EditDefaultsOnly, Category = "Game")
		float StartDelay = 2.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Game")
		FGameData GameData;


private:

	void SpawnPlayers();

	int32 CurrentRound = 1;
	int32 RoundCountDown = 0;
	FTimerHandle GameRoundTimerHandle;
	
	void StartRound();
	void GameTimerUpdate();

	void ResetPlayers();
	void ResetOnePlayer(AController* Controller);

	void CreateTeamsInfo();
	FLinearColor DetermineColorByTeamID(int32 TeamID) const;
	void SetPlayerColor(AController* Controller);

	void LogPlayerInfo();
	void StartRespawn(AController* Controller);

	UFUNCTION(Server,Reliable)
	void GameOver();

};
