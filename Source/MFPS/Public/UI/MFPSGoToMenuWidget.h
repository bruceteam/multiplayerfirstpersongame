// MFPS by Oleg Sutugin. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MFPSGoToMenuWidget.generated.h"

class UButton;

UCLASS()
class MFPS_API UMFPSGoToMenuWidget : public UUserWidget
{
	GENERATED_BODY()

protected:
	UPROPERTY(meta = (BindWidget))
		UButton* GoToMenuButton;

	virtual void NativeOnInitialized() override;
private:
	UFUNCTION()
		void OnGoToMenu();
};
