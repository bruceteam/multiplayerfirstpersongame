// Copyright Epic Games, Inc. All Rights Reserved.


#include "MFPSGameModeBase.h"
#include "Player/MFPSCharacter.h"
#include "Player/MFPSSpectatorPawn.h"
#include "UI/MFPSGameHUD.h"
#include "Player/MFPSPlayerController.h"
#include "MFPSGameStateBase.h"
#include "Player/MFPSPlayerState.h"
#include "MFPSUtils.h"
#include "Engine/World.h"
#include "EngineUtils.h"
#include "Components/MFPSRespawnComponent.h"
#include "Kismet/GameplayStatics.h"

DEFINE_LOG_CATEGORY_STATIC(LogMFPSGameMode, All, All);

AMFPSGameModeBase::AMFPSGameModeBase()
{
	DefaultPawnClass = AMFPSCharacter::StaticClass();
	PlayerControllerClass = AMFPSPlayerController::StaticClass();
	HUDClass = AMFPSGameHUD::StaticClass();
	PlayerStateClass = AMFPSPlayerState::StaticClass();
}

void AMFPSGameModeBase::StartPlay()
{
	Super::StartPlay();

	FTimerHandle DelayTimer;

	GetWorldTimerManager().SetTimer(DelayTimer, this, &AMFPSGameModeBase::StartPlayWithDelay, StartDelay, false);

}

void AMFPSGameModeBase::StartPlayWithDelay()	//new
{
	CreateTeamsInfo();

	CurrentRound = 1;
	StartRound();

	const auto GS = Cast<AMFPSGameStateBase>(GetWorld()->GetGameState());
	if(GS)
	GS->SetMatchState(EMFPSMatchState::InProgress);
}

void AMFPSGameModeBase::SpawnPlayers()		//do i need this without bots?
{
	if (!GetWorld())  return;

	for (int32 i = 0; i < GameData.PlayersNum - 1; ++i)
	{
		FActorSpawnParameters SpawnInfo;
		SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		GetWorld()->SpawnActor<AMFPSPlayerController>(SpawnInfo);
	}
}

void AMFPSGameModeBase::StartRound()
{
	//RoundCountDown = GameData.RoundTime;
	SetRoundCountDown(GameData.RoundTime);
	GetWorldTimerManager().SetTimer(GameRoundTimerHandle, this, &AMFPSGameModeBase::GameTimerUpdate, 1.0f, true);
}

void AMFPSGameModeBase::GameTimerUpdate()
{
	//Old 
	//UE_LOG(LogMFPSGameMode, Display, TEXT("Round %i of % i, time for next round %i"),CurrentRound, GameData.RoundsNum, RoundCountDown);

	SetRoundCountDown(-1);
	if (RoundCountDown == 0)
	{
		GetWorldTimerManager().ClearTimer(GameRoundTimerHandle);

		if (CurrentRound + 1 <= GameData.RoundsNum)
		{
			++CurrentRound;
			ResetPlayers();
			StartRound();
		}
		else
		{
			GameOver();
		}
	}
}

void AMFPSGameModeBase::ResetPlayers()
{
	if (!GetWorld()) return;

	for (auto It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		ResetOnePlayer(It->Get());
	}
}

void AMFPSGameModeBase::ResetOnePlayer(AController* Controller)
{
	if (Controller && Controller->GetPawn())
	{
		Controller->GetPawn()->Reset();
	}
	RestartPlayer(Controller);
	SetPlayerColor(Controller);
}

void AMFPSGameModeBase::CreateTeamsInfo()
{
	if (!GetWorld()) return;

	int32 TeamID = 1;
	for (auto It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		const auto Controller = It->Get();
		if (!Controller) continue;

		const auto PlayerState = Cast<AMFPSPlayerState>(Controller->PlayerState);
		if (!PlayerState) continue;

		PlayerState->SetTeamID(TeamID);
		PlayerState->SetTeamColor(DetermineColorByTeamID(TeamID));
		//PlayerState->SetPlayerName(Controller->IsPlayerController() ?

		SetPlayerColor(Controller);
		TeamID++;
	}
}

FLinearColor AMFPSGameModeBase::DetermineColorByTeamID(int32 TeamID) const
{
	if (TeamID - 1 < GameData.TeamColors.Num())
	{
		return GameData.TeamColors[TeamID - 1];
	}

	UE_LOG(LogMFPSGameMode, Display, TEXT("No color for TeamID : %i"), TeamID);
	return GameData.DefaultTeamColor;
}

void AMFPSGameModeBase::SetPlayerColor(AController* Controller)
{
	const auto Character = Cast<AMFPSCharacter>(Controller->GetPawn());

	if (!Character) return;

	const auto PlayerState = Cast<AMFPSPlayerState>(Controller->PlayerState);
	if (!PlayerState) return;

	Character->SetPlayerColor(PlayerState->GetTeamColor());
}

void AMFPSGameModeBase::Killed(AController* KillerController, AController* VictimController)
{
	const auto KillerPlayerState = KillerController ? Cast<AMFPSPlayerState>(KillerController->PlayerState) : nullptr;
	const auto VictimPlayerState = VictimController ? Cast<AMFPSPlayerState>(VictimController->PlayerState) : nullptr;

	if (KillerPlayerState)
	{
		if(KillerController!=VictimController)
		KillerPlayerState->AddKill();
	}

	if (VictimPlayerState)
	{
		VictimPlayerState->AddDeath();
	}

	StartRespawn(VictimController);
}


void AMFPSGameModeBase::SetRoundCountDown(int32 Value)
{
	RoundCountDown += Value;
	const auto GS = Cast<AMFPSGameStateBase>(GetWorld()->GetGameState());

	if (GS)
		GS->SetRoundTimer(RoundCountDown);
}

void AMFPSGameModeBase::LogPlayerInfo()
{
	if (!GetWorld()) return;

	for (auto It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
	{
		const auto Controller = It->Get();
		if (!Controller) continue;

		const auto PlayerState = Cast<AMFPSPlayerState>(Controller->PlayerState);
		if (!PlayerState) continue;

		PlayerState->LogInfo();
	}
}

void AMFPSGameModeBase::StartRespawn(AController* Controller)
{
	const bool RespawnAvailble = RoundCountDown > GameData.RespawnPlayersTime;

	const auto RespawnComponent = MFPSUtils::GetMFPSControllerComponent<UMFPSRespawnComponent>(Controller);
	if (!RespawnComponent || !RespawnAvailble) return;

	RespawnComponent->Respawn(GameData.RespawnPlayersTime);
}

void AMFPSGameModeBase::RespawnRequest(AController* Controller)
{
	ResetOnePlayer(Controller);
}


void AMFPSGameModeBase::GameOver_Implementation()
{
	//UE_LOG(LogTemp, Display, TEXT("----- GAME OVER -----"));
	LogPlayerInfo();

	OnGameOver.Broadcast();
	
	const auto GS = Cast<AMFPSGameStateBase>(GetWorld()->GetGameState());
	if(GS)
	GS->SetMatchState(EMFPSMatchState::GameOver);
}


