// MFPS by Oleg Sutugin. All rights reserved


#include "Menu/MFPSMenuGameModeBase.h"
#include "Menu/UI/MFPSMenuHUD.h"
#include "Menu/MFPSMenuPlayerController.h"

AMFPSMenuGameModeBase::AMFPSMenuGameModeBase()
{
	PlayerControllerClass = AMFPSMenuPlayerController::StaticClass();
	HUDClass = AMFPSMenuHUD::StaticClass();
}
