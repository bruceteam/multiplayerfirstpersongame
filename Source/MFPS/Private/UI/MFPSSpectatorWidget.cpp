// MFPS by Oleg Sutugin. All rights reserved


#include "UI/MFPSSpectatorWidget.h"
#include "MFPSUtils.h"
#include "Components/MFPSRespawnComponent.h"


bool UMFPSSpectatorWidget::GetRespawnTime(int32& RespawnTime) const
{
	const auto RespawnComponent = MFPSUtils::GetMFPSControllerComponent<UMFPSRespawnComponent>(GetOwningPlayer());
	if (!RespawnComponent) return false;

	RespawnTime = RespawnComponent->ReturnRespawnCountDown();

	return true;
}