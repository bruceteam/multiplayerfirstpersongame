// MFPS by Oleg Sutugin. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Weapon/MFPSBaseWeapon.h"
#include "MFPSRifleWeapon.generated.h"

class AMFPSProjectile;
class UNiagaraComponent;
class UAudioComponent;

UCLASS()
class MFPS_API AMFPSRifleWeapon : public AMFPSBaseWeapon
{
	GENERATED_BODY()

public:

	virtual void StartFire() override;
	virtual void StopFire() override;

protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Damage")
		float ShotSpeed = 0.2f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Damage")
		float BulletDispersion = 1.5f;

	UFUNCTION(Server, Reliable)
		virtual void MakeShot() override;

	virtual bool GetTraceData(FVector& TraceStart, FVector& TraceEnd) const override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Projectile")
		TSubclassOf<AMFPSProjectile> ProjectileClass;


private:
	FTimerHandle ShotTimerHandle;

	UPROPERTY()
		UAudioComponent* FireAudioComponent;

	UFUNCTION(Client, Reliable)
		void InitMuzzleFX();
	UFUNCTION(Client, Reliable)
		void SetMuzzleFXVisibility(bool Visible);

	UFUNCTION(Server, Reliable)
		void InitSoundFX();
	UFUNCTION(NetMulticast, Reliable)
		void M_InitSoundFX();

	UFUNCTION(Server, Reliable)
		void SetSoundFXVisibility(bool Visible);
	UFUNCTION(NetMulticast, Reliable)
		void M_SetSoundFXVisibility(bool Visible);
};
