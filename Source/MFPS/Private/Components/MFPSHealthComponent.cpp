// MFPS by Oleg Sutugin. All rights reserved


#include "Components/MFPSHealthComponent.h"
#include "GameFramework/Actor.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/Controller.h"
#include "Camera/CameraShakeBase.h"
#include "MFPS/MFPSGameModeBase.h"
#include "Net/UnrealNetwork.h"


UMFPSHealthComponent::UMFPSHealthComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

bool UMFPSHealthComponent::TryToAddHealth(float HealValue)
{
	if (Health == MaxHealth || bIsDead())
		return false;

	Health = (Health + HealValue >= MaxHealth) ? MaxHealth : Health + HealValue;

	OnHealthChanged.Broadcast(Health, false);
	return true;
}

void UMFPSHealthComponent::BeginPlay()
{
	Super::BeginPlay();
	check(MaxHealth > 0.0f);

	Health = MaxHealth;
	OnHealthChanged.Broadcast(Health, false);

	AActor* ComponentOwner = GetOwner();
	if (ComponentOwner)
	{
		ComponentOwner->OnTakeAnyDamage.AddDynamic(this, &UMFPSHealthComponent::OnTakeAnyDamage);
	}
}

void UMFPSHealthComponent::PlayCameraShake()
{
	if (bIsDead()) return;

	const auto Player = Cast<APawn>(GetOwner());
	if (!Player) return;

	const auto Controller = Player->GetController<APlayerController>();
	if (!Controller || !Controller->PlayerCameraManager) return;

	Controller->PlayerCameraManager->StartCameraShake(CameraShake);
}

void UMFPSHealthComponent::OnTakeAnyDamage_Implementation(AActor* DamagedActor, float Damage,
	const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	M_OnTakeAnyDamage(DamagedActor, Damage, DamageType, InstigatedBy, DamageCauser);
}


void UMFPSHealthComponent::M_OnTakeAnyDamage_Implementation(AActor* DamagedActor, float Damage,
	const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	if (Damage <= 0 || bIsDead()) return;

	Health = FMath::Clamp(Health - Damage, 0.0f, MaxHealth);
	PlayCameraShake();
	OnHealthChanged.Broadcast(Health, true);

	if (bIsDead())
	{
		//if(InstigatedBy!=GetOwner()->GetInstigatorController<APlayerController>())
		Killed(InstigatedBy);

		OnDeath.Broadcast();
	}
}

void UMFPSHealthComponent::Killed(AController* KillerController) 
{
	const auto GameMode =Cast<AMFPSGameModeBase>(GetWorld()->GetAuthGameMode());
	if (!GameMode) return;

	const auto Player = Cast<APawn>(GetOwner());
	const auto VictimController = Player ? Player->Controller : nullptr;

	GameMode->Killed(KillerController, VictimController);
}
