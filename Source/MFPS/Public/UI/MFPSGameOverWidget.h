// MFPS by Oleg Sutugin. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MFPSTypes.h"
#include "MFPSGameOverWidget.generated.h"

class UVerticalBox;
class UButton;

UCLASS()
class MFPS_API UMFPSGameOverWidget : public UUserWidget
{
	GENERATED_BODY()
	

protected:
	UPROPERTY(meta = (BindWidget))
		UVerticalBox* PlayerStatBox;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
		TSubclassOf<UUserWidget> PlayerStatRowWidgetClass;

	UPROPERTY(meta = (BindWidget))
		UButton* ResetLevelButton;

	//virtual bool Initialize() override;
	virtual void NativeOnInitialized() override;
private:
	void OnMatchStateChanged(EMFPSMatchState State);
	void UpdatePlayerStat();

	UFUNCTION()
		void OnResetLevel();
};
