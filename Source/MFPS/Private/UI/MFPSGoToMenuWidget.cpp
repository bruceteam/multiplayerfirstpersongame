// MFPS by Oleg Sutugin. All rights reserved


#include "UI/MFPSGoToMenuWidget.h"
#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"
#include "MFPSGameInstance.h"

void UMFPSGoToMenuWidget::NativeOnInitialized()
{
	if (GoToMenuButton)
	{
		GoToMenuButton->OnClicked.AddDynamic(this, &UMFPSGoToMenuWidget::OnGoToMenu);
	}

	Super::NativeOnInitialized();
}

void UMFPSGoToMenuWidget::OnGoToMenu()
{
	if (!GetWorld()) return;

	const auto MFPSGameInstance = GetWorld()->GetGameInstance<UMFPSGameInstance>();
	if (!MFPSGameInstance) return;

	if (MFPSGameInstance->GetMenuLevelName().IsNone())
	{
		return;
	}

	//if (GetOwningPlayer()->HasAuthority())
	//{
	//	GetWorld()->ServerTravel(MFPSGameInstance->GetMenuLevelName().ToString(), false);
	//}
	//else
	//{
		UGameplayStatics::OpenLevel(this, MFPSGameInstance->GetMenuLevelName());
	//}
}

