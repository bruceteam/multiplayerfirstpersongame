// MFPS by Oleg Sutugin. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "PickUp/MFPSBasePickUp.h"
#include "MFPSAmmoPickUp.generated.h"

class AMFPSBaseWeapon;

UCLASS()
class MFPS_API AMFPSAmmoPickUp : public AMFPSBasePickUp
{
	GENERATED_BODY()


protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PickUp", meta = (ClampMin = "1", ClampMax = "10"))
		int32 ClipsValue = 10;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PickUp")
		TSubclassOf<AMFPSBaseWeapon> WeaponType;

	virtual bool GivePickUpTo(APawn* Pawn) override;
};
