// MFPS by Oleg Sutugin. All rights reserved


#include "Player/MFPSPlayerController.h"
#include "MFPS/MFPSGameModeBase.h"
#include "MFPSGameStateBase.h"
#include "Components/MFPSRespawnComponent.h"

AMFPSPlayerController::AMFPSPlayerController()
{
	RespawnComponent = CreateDefaultSubobject<UMFPSRespawnComponent>("RespawnComponent");
}

void AMFPSPlayerController::BeginPlay()
{
	Super::BeginPlay();

	if (GetWorld())
	{
		const auto GameState = Cast<AMFPSGameStateBase>(GetWorld()->GetGameState());
		if (GameState)
		{
			GameState->OnMatchStateChanged.AddUObject(this, &AMFPSPlayerController::OnMatchStateChanged);
		}
	}
}

void AMFPSPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	if (!InputComponent) return;

	InputComponent->BindAction("PauseGame", IE_Pressed, this, &AMFPSPlayerController::OnPauseGame);
}

void AMFPSPlayerController::OnPauseGame()
{
	bIsPaused = !bIsPaused;

	if (bIsPaused)
	{
		bShowMouseCursor = true;
		//SetInputMode(FInputModeUIOnly());
	}

	if (!bIsPaused)
	{
		bShowMouseCursor = false;
		//SetInputMode(FInputModeUIOnly());
	}

}

void AMFPSPlayerController::OnMatchStateChanged(EMFPSMatchState State)
{
	if (State == EMFPSMatchState::InProgress)
	{
		SetInputMode(FInputModeGameOnly());
		bShowMouseCursor = false;
	}

	if (State == EMFPSMatchState::GameOver)
	{
		SetInputMode(FInputModeUIOnly());
		bShowMouseCursor = true;
	}
}


void AMFPSPlayerController::OnPossess_Implementation(APawn* InPawn)
{
	Super::OnPossess(InPawn);
	M_OnPossess(InPawn);

}

void AMFPSPlayerController::M_OnPossess_Implementation(APawn* InPawn)
{
	OnNewPawn.Broadcast(InPawn);
}