// MFPS by Oleg Sutugin. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MFPSBasePickUp.generated.h"

class USphereComponent;
class USoundCue;

UCLASS()
class MFPS_API AMFPSBasePickUp : public AActor
{
	GENERATED_BODY()
	
public:	

	AMFPSBasePickUp();

protected:
	UPROPERTY(VisibleAnywhere, Category = "PickUp")
		USphereComponent* CollisionComponent;

	UPROPERTY(VisibleAnywhere, Category = "PickUp")
		UStaticMeshComponent* StaticMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PickUp")
		float RespawnTime = 5.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
		USoundCue* PickUpSound;

	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

	virtual void BeginPlay() override;

	void PickUpWasTaken();
	void Respawn();

	virtual bool GivePickUpTo(APawn* Pawn);

	UFUNCTION(Server, Reliable)
	void PlayPickUpSound();
	UFUNCTION(NetMulticast, Reliable)
	void M_PlayPickUpSound();

public:	
	float RotationYaw = 0.0f;
	float LocationZ = 0.0f;
	bool UpFlag = true;

	virtual void Tick(float DeltaTime) override;
	void GenerateRotationYaw();
	void AddZLocation();
};
