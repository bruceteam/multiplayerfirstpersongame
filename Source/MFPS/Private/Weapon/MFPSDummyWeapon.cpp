// MFPS by Oleg Sutugin. All rights reserved


#include "Weapon/MFPSDummyWeapon.h"
#include "NiagaraFunctionLibrary.h"
#include "GameFramework/PlayerController.h"
#include "Engine/World.h"
#include "Net/UnrealNetwork.h"
#include "NiagaraComponent.h"


AMFPSDummyWeapon::AMFPSDummyWeapon()
{

	PrimaryActorTick.bCanEverTick = false;

	WeaponDummyMesh = CreateDefaultSubobject<USkeletalMeshComponent>("WeaponDummyMesh");
	SetRootComponent(WeaponDummyMesh);
}

void AMFPSDummyWeapon::StartFire()
{
	if (!bIsInfiniteEffect)
		SpawnMuzzleFX();
	else
		InitMuzzleFX();
}

void AMFPSDummyWeapon::StopFire()
{
	if (bIsInfiniteEffect)
		SetMuzzleFXVisibility(false);
}


void AMFPSDummyWeapon::BeginPlay()
{
	Super::BeginPlay();

}

void AMFPSDummyWeapon::SpawnMuzzleFX_Implementation()
{
	M_SpawnMuzzleFX();
}

void AMFPSDummyWeapon::M_SpawnMuzzleFX_Implementation()
{
	const auto Pawn = Cast<APawn>(GetOwner());
	if (!Pawn) return;
	if (!Pawn->IsLocallyControlled())
	{
		MuzzleFXComponent = UNiagaraFunctionLibrary::SpawnSystemAttached(MuzzleFX, //
			WeaponDummyMesh,													   //
			MuzzleSocketName,													   //
			FVector::ZeroVector,												   //
			FRotator::ZeroRotator,												   //
			EAttachLocation::SnapToTarget,										   //
			true);
	}
}

void AMFPSDummyWeapon::InitMuzzleFX()
{
	if (!MuzzleFXComponent)
	{
		SpawnMuzzleFX();
	}
	SetMuzzleFXVisibility(true);
}

void AMFPSDummyWeapon::SetMuzzleFXVisibility_Implementation(bool Visible)
{
	M_SetMuzzleFXVisibility(Visible);
}

void AMFPSDummyWeapon::M_SetMuzzleFXVisibility_Implementation(bool Visible)
{
	if (MuzzleFXComponent)
	{
		MuzzleFXComponent->SetPaused(!Visible);
		MuzzleFXComponent->SetVisibility(Visible, true);
	}
}

