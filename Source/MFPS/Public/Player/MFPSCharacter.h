// MFPS by Oleg Sutugin. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "MFPSCharacter.generated.h"

class UCameraComponent;
class UMFPSHealthComponent;
class UMFPSWeaponComponent;
class UWidgetComponent;
class USoundCue;

UCLASS()
class MFPS_API AMFPSCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	AMFPSCharacter(const FObjectInitializer& ObjInit);

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Camera")
		UCameraComponent* CameraComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "FPSMesh")
		USkeletalMeshComponent* SkeletalMeshFPS;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "HealthBar")
		UWidgetComponent* HealthWidgetComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "FPSMesh")
		USkeletalMeshComponent* SkeletalMeshTPS;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "HealthComponent")
		UMFPSHealthComponent* HealthComponent;

	UPROPERTY(Replicated,VisibleAnywhere, BlueprintReadWrite, Category = "WeaponComponent")  //here
		UMFPSWeaponComponent* WeaponComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health")
		float LifeSpanOnDeath = 3.0f;

	UFUNCTION(BlueprintCallable)
		float GetMovementDirection() const;

	UPROPERTY(EditDefaultsOnly, Category = "Material")
		FName MaterialColorName = "Paint Color";

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sound")
		USoundCue* DeathSound;

	UFUNCTION(NetMulticast, Reliable)
	void SetPlayerColor(const FLinearColor& Color);

	UFUNCTION(NetMulticast, Reliable)
		void GameOver();

protected:

	virtual void BeginPlay() override;
	
	//Movement and lookaround inputs
	void MoveForward(float Value);
	void MoveRight(float Value);

	void LookUP(float Value);

	UFUNCTION(Server, Reliable)
	void S_LookUPSync(FRotator RotationSync);
	UFUNCTION(NetMulticast, Reliable)
	void M_LookUPSync(FRotator RotationSync);

	void Turn(float Value);

	//������������� �� �������

	void OnDeath();
	void OnHealthChanged(float Health, bool bIsDamage);

	UFUNCTION(Server, Reliable)
		void S_PlayDeathSound();

	UFUNCTION(NetMulticast, Reliable)
		void M_PlayDeathSound();

	FRotator ControlRotationSynchronized;


public:

	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	//multicast
	UFUNCTION(NetMulticast, Unreliable)
		void PlayAMontage_Multicast (class UAnimMontage* AnimMontage,//
		USkeletalMeshComponent* MeshComponent, float InPlayRate = 1.f, FName StartSectionName = NAME_None);

};
