// MFPS by Oleg Sutugin. All rights reserved


#include "Weapon/MFPSRifleWeapon.h"
#include "Engine/World.h"
#include "Weapon/MFPSProjectile.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"
#include "NiagaraComponent.h"
#include "Components/AudioComponent.h"


DEFINE_LOG_CATEGORY_STATIC(LogRifleComponent, All, All);

void AMFPSRifleWeapon::StartFire()
{
	//UE_LOG(LogRifleComponent, Display, TEXT("Weapon firing!"));

	InitMuzzleFX();
	GetWorldTimerManager().SetTimer(ShotTimerHandle, this, &AMFPSRifleWeapon::MakeShot, ShotSpeed, true);
	MakeShot();
}

void AMFPSRifleWeapon::MakeShot_Implementation()
{
	if (!GetWorld() || IsAmmoEmpty())
	{
		StopFire();
		return;
	}

	FVector TraceStart, TraceEnd;
	if (!GetTraceData(TraceStart, TraceEnd))
	{
		StopFire();
		return;
	}

	FHitResult HitResult;
	MakeHit(HitResult, TraceStart, TraceEnd);

	const FVector EndPoint = HitResult.bBlockingHit ? HitResult.ImpactPoint : TraceEnd;
	const FVector Direction = (EndPoint - GetMuzzleWorldLocation()).GetSafeNormal();

	const FTransform SpawnTransform(GetActorRotation(), GetMuzzleWorldLocation());
	AMFPSProjectile* Projectile = GetWorld()->SpawnActorDeferred<AMFPSProjectile>(ProjectileClass, SpawnTransform);
	if (Projectile)
	{
		Projectile->SetShotDirection(Direction);
		Projectile->SetOwner(GetOwner());
		Projectile->FinishSpawning(SpawnTransform);
	}
	DecreaseAmmo();
}

void AMFPSRifleWeapon::StopFire()
{
	GetWorldTimerManager().ClearTimer(ShotTimerHandle);
	SetMuzzleFXVisibility(false);
}

bool AMFPSRifleWeapon::GetTraceData(FVector& TraceStart, FVector& TraceEnd) const
{
	FVector ViewLocation;
	FRotator ViewRotation;
	if (!GetPlayerViewPount(ViewLocation, ViewRotation)) return false;

	TraceStart = ViewLocation;
	const auto HalfRad = FMath::DegreesToRadians(BulletDispersion);
	const FVector ShootDirectiion = FMath::VRandCone(ViewRotation.Vector(), HalfRad);
	TraceEnd = TraceStart + ShootDirectiion * TraceMaxDistance;
	return true;
}

void AMFPSRifleWeapon::InitMuzzleFX_Implementation()
{
	if (!MuzzleFXComponent)
	{
		SpawnMuzzleFX();
	}
	if (!FireAudioComponent)
	{
		InitSoundFX();
	}

	SetMuzzleFXVisibility(true);
}
	
void AMFPSRifleWeapon::SetMuzzleFXVisibility_Implementation(bool Visible) 
{
	if (MuzzleFXComponent)
	{
		MuzzleFXComponent->SetPaused(!Visible);
		MuzzleFXComponent->SetVisibility(Visible, true);
	}

	if(FireAudioComponent)
	{ 
		SetSoundFXVisibility(Visible);
	}
}

void AMFPSRifleWeapon::InitSoundFX_Implementation()
{
	M_InitSoundFX();
}

void AMFPSRifleWeapon::M_InitSoundFX_Implementation()
{
	FireAudioComponent = UGameplayStatics::SpawnSoundAttached(FireSound, WeaponMesh, MuzzleSocketName);
}

void AMFPSRifleWeapon::SetSoundFXVisibility_Implementation(bool Visible)
{
	M_SetSoundFXVisibility(Visible);
}

void AMFPSRifleWeapon::M_SetSoundFXVisibility_Implementation(bool Visible)
{
	if (FireAudioComponent)
		FireAudioComponent->SetPaused(!Visible);
	//Visible ? FireAudioComponent->Play() : FireAudioComponent->SetPaused(true);
}