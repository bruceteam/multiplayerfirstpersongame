#pragma once

#include "MFPSTypes.generated.h"

//weapon 
class AMFPSBaseWeapon;
class AMFPSDummyWeapon;

DECLARE_MULTICAST_DELEGATE_OneParam(FOnEmptyClipSignature, AMFPSBaseWeapon*);

USTRUCT(BlueprintType)
struct FAmmoData
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
		int32 Bullets;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon", meta = (EditCondition = "!Infinite"))
		int32 Clips;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
		bool Infinite;
};

USTRUCT(BlueprintType)
struct FWeaponData
{
	GENERATED_USTRUCT_BODY()

public:
	//classes for weap
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
		TSubclassOf<AMFPSBaseWeapon> WeaponClass;

	UPROPERTY(EditDefaultsOnly, Category = "Animation")
		UAnimMontage* ReloadWeaponAnimMontage;
};

USTRUCT(BlueprintType)
struct FDummyWeaponData
{
	GENERATED_USTRUCT_BODY()

public:
	//classes for dummy
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
		TSubclassOf<AMFPSDummyWeapon> DummyWeaponClass;

	UPROPERTY(EditDefaultsOnly, Category = "Animation")
		UAnimMontage* ReloadDummyAnimMontage;
};

USTRUCT(BlueprintType)
struct FWeaponUIData
{
	GENERATED_USTRUCT_BODY()

public:
	//classes for dummy
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
		UTexture2D* MainIcon;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
		UTexture2D* CrossHairIcon;
};


//Health

DECLARE_MULTICAST_DELEGATE(FOnDeathSignature);
DECLARE_MULTICAST_DELEGATE_TwoParams(FOnHeatlthChangeSignature, float, bool);


//VFX

class UNiagaraSystem;
class USoundCue;

USTRUCT(BlueprintType)
struct FDecalData
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
		UMaterialInterface* Material;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
		FVector DecalSize = FVector(10.0f);

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
		float LifeTime = 5.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
		float FadeOutTime = 0.7f;
};

USTRUCT(BlueprintType)
struct FImpactFXData
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
		UNiagaraSystem* NiagaraEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
		FDecalData DecalData;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
		USoundCue* ImpactSound;
};

//Gameplay Data by gamemode
DECLARE_MULTICAST_DELEGATE(FOnGameOverSignature);
DECLARE_MULTICAST_DELEGATE_OneParam(FOnMatchStateChangedSignature, EMFPSMatchState);

USTRUCT(BlueprintType)
struct FGameData
{
	GENERATED_USTRUCT_BODY()

public:
	//seems i dont need that;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Game", meta = (clampMin = "1", ClampMax = "4"))
		int32 PlayersNum = 2;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Game", meta = (clampMin = "1", ClampMax = "10"))
		int32 RoundsNum = 4;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Game", meta = (clampMin = "3", ClampMax = "300"))
		int32 RoundTime = 10;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		FLinearColor DefaultTeamColor = FLinearColor::Red;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		TArray<FLinearColor> TeamColors;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Game", meta = (clampMin = "2", ClampMax = "30"))
		int32 RespawnPlayersTime = 5;
};

UENUM(BlueprintType)
enum class EMFPSMatchState : uint8
{
	WaitingToStart = 0,
	InProgress,
	GameOver
};
