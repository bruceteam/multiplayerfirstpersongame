// MFPS by Oleg Sutugin. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SpectatorPawn.h"
#include "MFPSSpectatorPawn.generated.h"


UCLASS()
class MFPS_API AMFPSSpectatorPawn : public ASpectatorPawn
{
	GENERATED_BODY()
	
public:


	virtual void BeginPlay() override;

	UFUNCTION(NetMulticast, Reliable)
		void GameOver();

};
