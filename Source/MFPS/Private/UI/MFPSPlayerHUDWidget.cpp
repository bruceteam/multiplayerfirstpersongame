// MFPS by Oleg Sutugin. All rights reserved


#include "UI/MFPSPlayerHUDWidget.h"
#include "Components/MFPSHealthComponent.h"
#include "Components/MFPSWeaponComponent.h"
#include "Player/MFPSCharacter.h"
#include "Player/MFPSPlayerController.h"
#include "MFPSUtils.h"

bool UMFPSPlayerHUDWidget::Initialize()
{
	if (GetOwningPlayer())
	{
		GetOwningPlayer()->GetOnNewPawnNotifier().AddUObject(this, &UMFPSPlayerHUDWidget::OnNewPawn);
		OnNewPawn(GetOwningPlayerPawn());
	}

	return Super::Initialize();
}

float UMFPSPlayerHUDWidget::GetHealthPercent() const
{
	const auto HealthComponent = MFPSUtils::GetMFPSPlayerComponent<UMFPSHealthComponent>(GetOwningPlayerPawn());
	if (!HealthComponent) return 0.0f;

	return HealthComponent->GetHealthPercent();
}

bool UMFPSPlayerHUDWidget::GetWeaponUIData(FWeaponUIData& UIData) const
{
	const auto WeaponComponent = MFPSUtils::GetMFPSPlayerComponent<UMFPSWeaponComponent>(GetOwningPlayerPawn());
	if (!WeaponComponent) return false;

	return WeaponComponent->GetWeaponUIData(UIData);

}

bool UMFPSPlayerHUDWidget::GetWeaponAmmoData(FAmmoData& AmmoData) const
{
	const auto WeaponComponent = MFPSUtils::GetMFPSPlayerComponent<UMFPSWeaponComponent>(GetOwningPlayerPawn());
	if (!WeaponComponent) return false;

	return WeaponComponent->GetWeaponAmmoData(AmmoData);

}

bool UMFPSPlayerHUDWidget::IsPlayerAlive() const
{
	const auto HealthComponent = MFPSUtils::GetMFPSPlayerComponent<UMFPSHealthComponent>(GetOwningPlayerPawn());
	return HealthComponent && HealthComponent->bIsAlive();
}

bool UMFPSPlayerHUDWidget::IsPlayerSpectating() const
{
	const auto Controller = GetOwningPlayer();
	return Controller && Controller->GetStateName() == NAME_Spectating;
}

void UMFPSPlayerHUDWidget::OnHealthChanged(float Health, bool bIsDamage)
{
	if (bIsDamage)
	{
		OnTakeDamage();
	}
}

void UMFPSPlayerHUDWidget::OnNewPawn(APawn* NewPawn)
{
	const auto HealthComponent = MFPSUtils::GetMFPSPlayerComponent<UMFPSHealthComponent>(NewPawn);
	if (HealthComponent/* && !HealthComponent->OnHealthChanged.IsBoundToObject(this)*/)
	{
		HealthComponent->OnHealthChanged.AddUObject(this, &UMFPSPlayerHUDWidget::OnHealthChanged);
	}
}

