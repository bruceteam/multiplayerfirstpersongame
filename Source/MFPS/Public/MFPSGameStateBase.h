// MFPS by Oleg Sutugin. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "MFPSTypes.h"
#include "GameFramework/GameStateBase.h"
#include "MFPSGameStateBase.generated.h"


UCLASS()
class MFPS_API AMFPSGameStateBase : public AGameStateBase
{
	GENERATED_BODY()
	

public:
	UFUNCTION(NetMulticast, Reliable)
	void SetRoundTimer(int32 Time);

	UFUNCTION(BlueprintCallable) 
	int32 GetRoundTimer() const { return RoundTimer; }

	UFUNCTION(NetMulticast,Reliable)
	void SetMatchState(EMFPSMatchState State);

	FOnMatchStateChangedSignature OnMatchStateChanged;
private:
	int32 RoundTimer;

	EMFPSMatchState CurrentState = EMFPSMatchState::WaitingToStart;
};
