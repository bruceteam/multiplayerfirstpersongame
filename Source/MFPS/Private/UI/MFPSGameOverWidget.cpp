// MFPS by Oleg Sutugin. All rights reserved


#include "UI/MFPSGameOverWidget.h"
#include "MFPSGameStateBase.h"
#include "MFPSUtils.h"
#include "Player/MFPSPlayerState.h"
#include "UI/MFPSPlayerStatRowWidget.h"
#include "MFPS/MFPSGameModeBase.h"
#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"
#include "Components/VerticalBox.h"

//bool UMFPSGameOverWidget::Initialize()
//{
//
//	if (GetWorld())
//	{
//		const auto GS = Cast<AMFPSGameStateBase>(GetWorld()->GetGameState());
//		if (GS)
//		{
//			GS->OnMatchStateChanged.AddUObject(this, &UMFPSGameOverWidget::OnMatchStateChanged);
//		}
//	}
//
//	if (ResetLevelButton)
//	{
//		//if (!GetOwningPlayer()->HasAuthority()) return;
//
//		ResetLevelButton->OnClicked.AddDynamic(this, &UMFPSGameOverWidget::OnResetLevel);
//	}
//
//	return Super::Initialize();
//}

void UMFPSGameOverWidget::NativeOnInitialized()
{

	if (GetWorld())
	{
		const auto GS = Cast<AMFPSGameStateBase>(GetWorld()->GetGameState());
		if (GS)
		{
			GS->OnMatchStateChanged.AddUObject(this, &UMFPSGameOverWidget::OnMatchStateChanged);
		}
	}

	if (ResetLevelButton)
	{
		//if (!GetOwningPlayer()->HasAuthority()) return;

		ResetLevelButton->OnClicked.AddDynamic(this, &UMFPSGameOverWidget::OnResetLevel);
	}

	Super::NativeOnInitialized();
}

void UMFPSGameOverWidget::OnMatchStateChanged(EMFPSMatchState State)
{
	if (State == EMFPSMatchState::GameOver)
	{
		UpdatePlayerStat();
	}
}

void UMFPSGameOverWidget::UpdatePlayerStat()
{
	if (!GetWorld() || !PlayerStatBox) return;

	PlayerStatBox->ClearChildren();
	TArray<APlayerState*> PlayerStates = GetWorld()->GetGameState()->PlayerArray;
	for (auto ObjPtrPlayerState : PlayerStates)
	{
		const auto PlayerState = Cast<AMFPSPlayerState>(ObjPtrPlayerState);
		if (!PlayerState) continue;

		const auto PlayerStatRowWidget = CreateWidget<UMFPSPlayerStatRowWidget>(GetWorld(), PlayerStatRowWidgetClass);
		if (!PlayerStatRowWidget) continue;

		PlayerStatRowWidget->SetPlayerName(FText::FromString(PlayerState->GetPlayerName()));
		PlayerStatRowWidget->SetKills(MFPSUtils::TextFromInt(PlayerState->GetKills()));
		PlayerStatRowWidget->SetDeaths(MFPSUtils::TextFromInt(PlayerState->GetDeaths()));

		PlayerStatBox->AddChild(PlayerStatRowWidget);
	}
}

void UMFPSGameOverWidget::OnResetLevel()
{
	//const FName CurrentLevelName = "GrayBox";
	const FString CurrentLevelName = UGameplayStatics::GetCurrentLevelName(this);
	UE_LOG(LogTemp, Display, TEXT("TryToRestartLevel %s"), *CurrentLevelName);
	if (!GetWorld()) return;

	const auto GM = Cast<AMFPSGameModeBase>(GetWorld()->GetAuthGameMode());
	if (!GM) return;

	GM->GetWorld()->ServerTravel(CurrentLevelName,false);
//	UGameplayStatics::OpenLevel(this, FName(CurrentLevelName));
}
