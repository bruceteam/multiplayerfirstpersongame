// MFPS by Oleg Sutugin. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MFPSPlayerStatRowWidget.generated.h"


class UImage;
class UTextBlock;


UCLASS()
class MFPS_API UMFPSPlayerStatRowWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	void SetPlayerName(const FText& Text);
	void SetKills(const FText& Text);
	void SetDeaths(const FText& Text);

protected:
	UPROPERTY(meta = (BindWidget))
		UTextBlock* PlayerNameTextBlock;

	UPROPERTY(meta = (BindWidget))
		UTextBlock* KillsTextBlock;

	UPROPERTY(meta = (BindWidget))
		UTextBlock* DeathsTextBlock;
};
