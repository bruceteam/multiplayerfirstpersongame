// MFPS by Oleg Sutugin. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "MFPSTypes.h"
#include "MFPSHealthComponent.generated.h"

class UCameraShakeBase;

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class MFPS_API UMFPSHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UMFPSHealthComponent();

	UFUNCTION(BlueprintCallable, Category = "Health")
		float GetHealth() { return Health; }

	UFUNCTION(BlueprintCallable, Category = "Health")
		bool bIsDead() const { return Health <= 0; }

	UFUNCTION(BlueprintCallable, Category = "Health")
		bool bIsAlive() const { return Health > 0; }

	UFUNCTION( BlueprintCallable, Category = "Health")
		float GetHealthPercent() const { return Health / MaxHealth; }

	bool TryToAddHealth(float HealValue);

	FOnDeathSignature OnDeath;
	FOnHeatlthChangeSignature OnHealthChanged;
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "Health")
		float MaxHealth = 100.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "VFX")
		TSubclassOf<UCameraShakeBase> CameraShake;


	virtual void BeginPlay() override;


private:
	float Health;

	UFUNCTION(Server,Reliable)
		void OnTakeAnyDamage(
			AActor* DamagedActor, float Damage, const class UDamageType* DamageType,
			class AController* InstigatedBy, AActor* DamageCauser);

	UFUNCTION(NetMulticast,Reliable)
		void M_OnTakeAnyDamage(
			AActor* DamagedActor, float Damage, const class UDamageType* DamageType,
			class AController* InstigatedBy, AActor* DamageCauser);

	void PlayCameraShake();
	void Killed(AController* KillerController);
};
