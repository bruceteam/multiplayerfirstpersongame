// MFPS by Oleg Sutugin. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Weapon/MFPSBaseWeapon.h"
#include "MFPSRailGunWeapon.generated.h"

class UMFPSWeaponFXComponent;
class UNiagaraSystem;
class USoundCue;

UCLASS()
class MFPS_API AMFPSRailGunWeapon : public AMFPSBaseWeapon
{
	GENERATED_BODY()

public:
	AMFPSRailGunWeapon();

	virtual void StartFire() override;

	UFUNCTION(Server, Reliable)
		void S_MakeShot();
	UFUNCTION(NetMulticast, Reliable)
		void M_MakeShot();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Damage")
		float DamageAmount = 10.0f;

	virtual void BeginPlay() override;
protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
		UMFPSWeaponFXComponent* WeaponFXComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
		UNiagaraSystem* TraceFX;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sound")
		USoundCue* NoAmmoSound;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
		FString TraceTargetName = "TraceTarget";
private:
	void MakeDamage(const FHitResult& HitResult);

	UFUNCTION(Server, Reliable)
		void S_SpawnTraceFX(const FVector& TraceStart, const FVector& TraceEnd);
	UFUNCTION(NetMulticast, Reliable)
		void M_SpawnTraceFX(const FVector& TraceStart, const FVector& TraceEnd);

	AController* GetController() const;
};
