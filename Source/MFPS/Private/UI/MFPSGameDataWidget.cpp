// MFPS by Oleg Sutugin. All rights reserved


#include "UI/MFPSGameDataWidget.h"
#include "Player/MFPSPlayerState.h"
#include "MFPSGameStateBase.h"

int32 UMFPSGameDataWidget::GetRoundTimeRemaining() const
{
	if(!GetWorld()) return 0;

	const auto GS = Cast<AMFPSGameStateBase>(GetWorld()->GetGameState());
	return GS ? GS->GetRoundTimer() : 0;
}

int32 UMFPSGameDataWidget::GetPlayerKills() const
{
	const auto PlayerState = Cast<AMFPSPlayerState>(GetOwningPlayer()->PlayerState);
	return PlayerState ? PlayerState->GetKills() : 0;	
}

int32 UMFPSGameDataWidget::GetPlayerDeaths() const
{
	const auto PlayerState = Cast<AMFPSPlayerState>(GetOwningPlayer()->PlayerState);
	return PlayerState ? PlayerState->GetDeaths() : 0;
}