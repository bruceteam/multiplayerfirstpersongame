// MFPS by Oleg Sutugin. All rights reserved


#include "Player/MFPSSpectatorPawn.h"
#include "MFPS/MFPSGameModeBase.h"
#include "Net/UnrealNetwork.h"



void AMFPSSpectatorPawn::BeginPlay()
{
	Super::BeginPlay();

	 const auto GM = Cast<AMFPSGameModeBase>(GetWorld()->GetAuthGameMode());
	if (GM)
	{
		UE_LOG(LogTemp, Display, TEXT("Podpisalsya"));
		GM->OnGameOver.AddUObject(this, &AMFPSSpectatorPawn::GameOver);
	}
}


void AMFPSSpectatorPawn::GameOver_Implementation()
{
	this->TurnOff();
	this->DisableInput(nullptr);
}