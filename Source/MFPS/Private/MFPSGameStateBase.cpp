// MFPS by Oleg Sutugin. All rights reserved


#include "MFPSGameStateBase.h"

void AMFPSGameStateBase::SetRoundTimer_Implementation(int32 Time) 
{
	RoundTimer = Time; 
}

void AMFPSGameStateBase::SetMatchState_Implementation(EMFPSMatchState State) 
{
	if (CurrentState == State) return;

	CurrentState = State; 
	OnMatchStateChanged.Broadcast(CurrentState);
}