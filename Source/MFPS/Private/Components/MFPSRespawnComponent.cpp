// MFPS by Oleg Sutugin. All rights reserved


#include "Components/MFPSRespawnComponent.h"
#include "Net/UnrealNetwork.h"
#include "MFPS/MFPSGameModeBase.h"

UMFPSRespawnComponent::UMFPSRespawnComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UMFPSRespawnComponent::Respawn(int32 RespawnTime)
{
	if (!GetWorld()) return;

	RespawnCountDown = RespawnTime;
	GetWorld()->GetTimerManager().SetTimer(RespawnTimerHandle, this, &UMFPSRespawnComponent::RespawnTimerUpdate, 1.0f, true);
}

void UMFPSRespawnComponent::RespawnTimerUpdate()
{
	if (--RespawnCountDown == 0)
	{
		if(!GetWorld()) return;
		GetWorld()->GetTimerManager().ClearTimer(RespawnTimerHandle);

		const auto GameMode = Cast<AMFPSGameModeBase>(GetWorld()->GetAuthGameMode());
		if (!GameMode) return;

		GameMode->RespawnRequest(Cast<AController>(GetOwner()));
	}
}


void UMFPSRespawnComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const   //here
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Here we list the variables we want to replicate
	DOREPLIFETIME(UMFPSRespawnComponent, RespawnCountDown);

}

