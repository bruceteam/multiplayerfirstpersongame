// MFPS by Oleg Sutugin. All rights reserved


#include "PickUp/MFPSAmmoPickUp.h"
#include "Components/MFPSWeaponComponent.h"
#include "Components/MFPSHealthComponent.h"
#include "MFPSUtils.h"

DEFINE_LOG_CATEGORY_STATIC(LogWeaponPickUp, All, All);

bool AMFPSAmmoPickUp::GivePickUpTo(APawn* Pawn)
{
	const auto HealthComponent = MFPSUtils::GetMFPSPlayerComponent<UMFPSHealthComponent>(Pawn);
	if (!HealthComponent || HealthComponent->bIsDead()) return false;

	const auto WeaponComponent = MFPSUtils::GetMFPSPlayerComponent<UMFPSWeaponComponent>(Pawn);
	if (!WeaponComponent) return false;

	return WeaponComponent->TryToAddAmmo(WeaponType, ClipsValue);
}
