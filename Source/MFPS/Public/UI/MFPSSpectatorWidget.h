// MFPS by Oleg Sutugin. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MFPSSpectatorWidget.generated.h"


UCLASS()
class MFPS_API UMFPSSpectatorWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable, Category = "UI")
		bool GetRespawnTime(int32& RespawnTime) const;
};
