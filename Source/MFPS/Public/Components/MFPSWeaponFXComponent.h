// MFPS by Oleg Sutugin. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "MFPSTypes.h"
#include "MFPSWeaponFXComponent.generated.h"


class UNiagaraSystem;
class UPhysicalMaterial;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MFPS_API UMFPSWeaponFXComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UMFPSWeaponFXComponent();

	UFUNCTION(Server,Reliable)
	void S_PlayImpactFX(const FHitResult& Hit);

	UFUNCTION(NetMulticast,Reliable)
	void M_PlayImpactFX(const FHitResult& Hit);

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
		FImpactFXData DefaultImpactData;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
		TMap<UPhysicalMaterial*, FImpactFXData> ImpactDataMap;
};
