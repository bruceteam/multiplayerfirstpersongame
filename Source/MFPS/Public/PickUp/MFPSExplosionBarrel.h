// MFPS by Oleg Sutugin. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MFPSExplosionBarrel.generated.h"

class UNiagaraSystem;
class USoundCue;

UCLASS()
class MFPS_API AMFPSExplosionBarrel : public AActor
{
	GENERATED_BODY()

public:

	AMFPSExplosionBarrel();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
		UStaticMeshComponent* BarrelMesh;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Behavior")
		float RespawnTime = 10.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Behavior")
		float DamageByExplosion = 100.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Behavior")
		float ExplosionRadius = 500.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Behavior")
		UNiagaraSystem* ExplosionFX;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
		USoundCue* ExplosionSound;

	virtual void BeginPlay() override;

	UFUNCTION(Server, Reliable)
		void OnTakeDamage(
			AActor* DamagedActor, float Damage, const class UDamageType* DamageType,
			class AController* InstigatedBy, AActor* DamageCauser);

	UFUNCTION(NetMulticast, Reliable)
		void M_OnTakeDamage(
			AActor* DamagedActor, float Damage, const class UDamageType* DamageType,
			class AController* InstigatedBy, AActor* DamageCauser);

	FTimerHandle VisibilityTimer;

	void ToggleVisibility();

};
