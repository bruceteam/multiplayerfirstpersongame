// MFPS by Oleg Sutugin. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MFPSDummyWeapon.generated.h"

class UNiagaraSystem;
class UNiagaraComponent;


UCLASS()
class MFPS_API AMFPSDummyWeapon : public AActor
{
	GENERATED_BODY()

public:
	AMFPSDummyWeapon();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Component")
		USkeletalMeshComponent* WeaponDummyMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Component")
		FName MuzzleSocketName = "MuzzleSocket";

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
		UNiagaraSystem* MuzzleFX;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
		bool bIsInfiniteEffect;

	

	virtual void StartFire();
	virtual void StopFire();


protected:
	virtual void BeginPlay() override;

	UNiagaraComponent* MuzzleFXComponent;
	
	UFUNCTION(Server, Reliable)
	void SpawnMuzzleFX();

	UFUNCTION(NetMulticast , Reliable)
	void M_SpawnMuzzleFX();

	void InitMuzzleFX();

	UFUNCTION(Server, Reliable)
	void SetMuzzleFXVisibility(bool Visible);
	UFUNCTION(NetMulticast, Reliable)
	void M_SetMuzzleFXVisibility(bool Visible);
};
