// MFPS by Oleg Sutugin. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "MFPSPlayerState.generated.h"


UCLASS()
class MFPS_API AMFPSPlayerState : public APlayerState
{
	GENERATED_BODY()
	

public:
	void SetTeamID(int32 ID) { TeamID = ID; }
	int32 GetTeamID() const { return TeamID; }

	void SetTeamColor(const FLinearColor& Color) { TeamColor = Color; }
	FLinearColor GetTeamColor() const { return TeamColor; }

	UFUNCTION(NetMulticast, Reliable)
		void AddKill();
	int32 GetKills() const{ return KillsNum; }	

	UFUNCTION(NetMulticast, Reliable)
		void AddDeath();
	int32 GetDeaths() const { return DeathNum; }


	void LogInfo();
private:
	int32 TeamID;
	FLinearColor TeamColor;
	
	int32 KillsNum = 0;
	int32 DeathNum = 0;
};
