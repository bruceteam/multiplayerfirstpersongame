// MFPS by Oleg Sutugin. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Animations/MFPSAnimNotify.h"
#include "MFPSReloadFinishedAnimNotify.generated.h"


UCLASS()
class MFPS_API UMFPSReloadFinishedAnimNotify : public UMFPSAnimNotify
{
	GENERATED_BODY()
	
};
