// MFPS by Oleg Sutugin. All rights reserved


#include "PickUp/MFPSBasePickUp.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"

DEFINE_LOG_CATEGORY_STATIC(LogPickUp, All, All);

AMFPSBasePickUp::AMFPSBasePickUp()
{
	PrimaryActorTick.bCanEverTick = true;

	CollisionComponent = CreateDefaultSubobject<USphereComponent>("CollisionComponent");
	CollisionComponent->InitSphereRadius(50.0f);
	CollisionComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	CollisionComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
	SetRootComponent(CollisionComponent);

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("StaticMesh");
	StaticMesh->SetupAttachment(CollisionComponent);
}


void AMFPSBasePickUp::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);

	const auto Pawn = Cast<APawn>(OtherActor);
	if (Pawn && GivePickUpTo(Pawn))
	{
		PickUpWasTaken();
	}
}

void AMFPSBasePickUp::BeginPlay()
{
	Super::BeginPlay();

	check(CollisionComponent);

	LocationZ = this->GetActorLocation().Z;
	GenerateRotationYaw();

}


void AMFPSBasePickUp::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	AddActorLocalRotation(FRotator(0.0f, RotationYaw, 0.0f));
	AddZLocation();
}

void AMFPSBasePickUp::PickUpWasTaken()
{
	CollisionComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	GetRootComponent()->SetVisibility(false, true);
	FTimerHandle RespawnTimerHandle;
	GetWorldTimerManager().SetTimer(RespawnTimerHandle, this, &AMFPSBasePickUp::Respawn, RespawnTime);
	PlayPickUpSound();
}

void AMFPSBasePickUp::PlayPickUpSound_Implementation()
{
	M_PlayPickUpSound();
}

void AMFPSBasePickUp::M_PlayPickUpSound_Implementation()
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), PickUpSound, GetActorLocation());
}

void AMFPSBasePickUp::Respawn()
{
	GenerateRotationYaw();
	CollisionComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
	GetRootComponent()->SetVisibility(true, true);
}

bool AMFPSBasePickUp::GivePickUpTo(APawn* Pawn)
{
	return false;
}

void AMFPSBasePickUp::GenerateRotationYaw()
{
	const auto Direction = FMath::RandBool() ? 1.0f : -1.0f;
	RotationYaw = FMath::RandRange(1.0f, 2.0f) * Direction;
}

void AMFPSBasePickUp::AddZLocation()
{
	if (UpFlag)
	{
		SetActorLocation(FVector(GetActorLocation().X, GetActorLocation().Y, GetActorLocation().Z + 1.0f));
		if (GetActorLocation().Z > LocationZ + 30.f)
		{
			UpFlag = false;
		}
	}
	else
	{
		SetActorLocation(FVector(GetActorLocation().X, GetActorLocation().Y, GetActorLocation().Z - 1.0f));
		if (GetActorLocation().Z < LocationZ - 30.f)
		{
			UpFlag = true;
		}
	}
}
