// MFPS by Oleg Sutugin. All rights reserved


#include "PickUp/MFPSExplosionBarrel.h"
#include "NiagaraFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"

AMFPSExplosionBarrel::AMFPSExplosionBarrel()
{
	PrimaryActorTick.bCanEverTick = false;

	BarrelMesh = CreateDefaultSubobject<UStaticMeshComponent>("BarrelMesh");
	SetRootComponent(BarrelMesh);
}


void AMFPSExplosionBarrel::BeginPlay()
{
	Super::BeginPlay();

	OnTakeAnyDamage.AddDynamic(this, &AMFPSExplosionBarrel::OnTakeDamage);
}


void AMFPSExplosionBarrel::OnTakeDamage_Implementation(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	M_OnTakeDamage(DamagedActor, Damage, DamageType, InstigatedBy, DamageCauser);
}

void AMFPSExplosionBarrel::M_OnTakeDamage_Implementation(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	UE_LOG(LogTemp, Display, TEXT("Barrel BlowedUP!!!"));

	UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(),    //
		ExplosionFX,			     							  //
		this->GetActorLocation()								  //
	);

	UGameplayStatics::ApplyRadialDamage(
		GetWorld(),						//
		DamageByExplosion,				//
		this->GetActorLocation(),		//
		ExplosionRadius,				//
		UDamageType::StaticClass(),		//
		{},								//
		this,							//
		InstigatedBy,					//
		true							//
	);

	BarrelMesh->SetVisibility(false, true);
	BarrelMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);

	GetWorldTimerManager().SetTimer(VisibilityTimer,this, &AMFPSExplosionBarrel::ToggleVisibility, RespawnTime, false);

	UGameplayStatics::PlaySoundAtLocation(GetWorld(), ExplosionSound, GetActorLocation());
}

void AMFPSExplosionBarrel::ToggleVisibility()
{
	BarrelMesh->SetVisibility(true, true);
	BarrelMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
}