// MFPS by Oleg Sutugin. All rights reserved


#include "Weapon/MFPSBaseWeapon.h"
#include "Components/SkeletalMeshComponent.h"
#include "Engine/World.h"
#include "GameFramework/Character.h"
#include "GameFramework/Controller.h"
#include "Kismet/GameplayStatics.h"
#include "Components/MFPSWeaponComponent.h"
#include "Net/UnrealNetwork.h"
#include "Engine/EngineTypes.h"
#include "Player/MFPSCharacter.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"
#include "DrawDebugHelpers.h"

DEFINE_LOG_CATEGORY_STATIC(LogWeapon, All, All);

AMFPSBaseWeapon::AMFPSBaseWeapon()
{
	PrimaryActorTick.bCanEverTick = false;

	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>("SkeletalMesh");
	SetRootComponent(WeaponMesh);
}

void AMFPSBaseWeapon::BeginPlay()
{
	Super::BeginPlay();

	check(WeaponMesh);
	checkf(DefaultAmmo.Bullets > 0, TEXT("Bullets could't be less or equal zero! Fill In BP!"));
	checkf(DefaultAmmo.Clips > 0, TEXT("Clips could't be less or equal zero! Fill In BP!"));
	CurrentAmmo = DefaultAmmo;
}

void AMFPSBaseWeapon::StartFire_Implementation()
{
	//removed
}

void AMFPSBaseWeapon::StopFire_Implementation()
{
	//removed
}

void AMFPSBaseWeapon::MakeShot()
{
	//removed
}

APlayerController* AMFPSBaseWeapon::GetPlayerController() const
{
	const auto Player = Cast<ACharacter>(GetOwner());
	if (!Player) return nullptr;

	return Player->GetController<APlayerController>();
}

bool AMFPSBaseWeapon::GetPlayerViewPount(FVector& ViewLocation, FRotator& ViewRotation) const
{
	const auto Controller = GetPlayerController();
	if (!Controller) return false;

	Controller->GetPlayerViewPoint(ViewLocation, ViewRotation);
	return true;
}

FVector AMFPSBaseWeapon::GetMuzzleWorldLocation() const
{
	return WeaponMesh->GetSocketLocation(MuzzleSocketName);
}

bool AMFPSBaseWeapon::GetTraceData(FVector& TraceStart, FVector& TraceEnd) const
{
	FVector ViewLocation;
	FRotator ViewRotation;
	if (!GetPlayerViewPount(ViewLocation, ViewRotation)) return false;

	TraceStart = ViewLocation;
	const FVector ShootDirectiion = ViewRotation.Vector();
	TraceEnd = TraceStart + ShootDirectiion * TraceMaxDistance;
	return true;
}

void AMFPSBaseWeapon::MakeHit(FHitResult& HitResult, const FVector& TraceStart, FVector& TraceEnd)
{
	if (!GetWorld()) return;

	FCollisionQueryParams CollisionParams;
	CollisionParams.AddIgnoredActor(GetOwner());
	CollisionParams.bReturnPhysicalMaterial = true;

	GetWorld()->LineTraceSingleByChannel(HitResult, TraceStart, TraceEnd, ECollisionChannel::ECC_Visibility, CollisionParams);
}

void AMFPSBaseWeapon::DecreaseAmmo()
{
	if (GetOwner()->HasAuthority())
	{

		if (CurrentAmmo.Bullets == 0)
		{
			UE_LOG(LogWeapon, Display, TEXT("NO MORE Bullets in clip!"));
			return;
		}

		CurrentAmmo.Bullets--;
		//LogAmmo();

		if (IsClipEmpty() && !IsAmmoEmpty())
		{	
			StopFire();
			OnEmptyClip.Broadcast(this);
		}
	}
}


bool AMFPSBaseWeapon::IsAmmoEmpty() const
{
	return !CurrentAmmo.Infinite && CurrentAmmo.Clips == 0 && IsClipEmpty();
}

bool AMFPSBaseWeapon::IsClipEmpty() const
{
	return CurrentAmmo.Bullets == 0;
}

bool AMFPSBaseWeapon::IsAmmoFull() const
{
	return CurrentAmmo.Clips == DefaultAmmo.Clips && CurrentAmmo.Bullets == DefaultAmmo.Bullets;
}

void AMFPSBaseWeapon::ChangeClip()
{
	if (GetOwner()->HasAuthority())
	{

		if (!CurrentAmmo.Infinite)
		{
			if (CurrentAmmo.Clips == 0)
			{
				UE_LOG(LogWeapon, Display, TEXT("NO MORE CLIPS!"));
				return;
			}
			CurrentAmmo.Clips--;
		}

		CurrentAmmo.Bullets = DefaultAmmo.Bullets;
		UE_LOG(LogWeapon, Display, TEXT("***Clip Changed***"));
	}
}



//debug
void AMFPSBaseWeapon::LogAmmo()
{
	FString AmmoInfo = "Ammo: " + FString::FromInt(CurrentAmmo.Bullets) + " / ";
	AmmoInfo += CurrentAmmo.Infinite ? "Infinite" : "Clips " + FString::FromInt(CurrentAmmo.Clips);

	UE_LOG(LogWeapon, Display, TEXT("%s"), *AmmoInfo);
}


bool AMFPSBaseWeapon::CanReload() const
{
	return CurrentAmmo.Bullets < DefaultAmmo.Bullets&& CurrentAmmo.Clips > 0;
}

bool AMFPSBaseWeapon::TryToAddAmmo(int32 ClipsValue)
{
	if (CurrentAmmo.Infinite || IsAmmoFull() || ClipsValue <= 0) return false;

	if (IsAmmoEmpty())
	{
		CurrentAmmo.Clips = FMath::Clamp(CurrentAmmo.Clips + ClipsValue, 0, DefaultAmmo.Clips + 1);
		OnEmptyClip.Broadcast(this);
	}
	else if (CurrentAmmo.Clips < DefaultAmmo.Clips)
	{
		const auto NextClipsAmount = CurrentAmmo.Clips + ClipsValue;
		if (DefaultAmmo.Clips - NextClipsAmount >= 0)
		{
			CurrentAmmo.Clips = NextClipsAmount;
		}
		else
		{
			CurrentAmmo.Clips = DefaultAmmo.Clips;
			CurrentAmmo.Bullets = DefaultAmmo.Bullets;
		}
	}
	else
	{
		CurrentAmmo.Bullets = DefaultAmmo.Bullets;
	}
	return true;
}

void AMFPSBaseWeapon::SpawnMuzzleFX_Implementation()
{
	MuzzleFXComponent = UNiagaraFunctionLibrary::SpawnSystemAttached(MuzzleFX,//
		WeaponMesh,															   //
		MuzzleSocketName,													   //
		FVector::ZeroVector,												   //
		FRotator::ZeroRotator,												   //
		EAttachLocation::SnapToTarget,										   //
		true);
}

void AMFPSBaseWeapon::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const   //here
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Here we list the variables we want to replicate
	DOREPLIFETIME(AMFPSBaseWeapon, CurrentAmmo);

}
