// MFPS by Oleg Sutugin. All rights reserved


#include "UI/MFPSHealthBarWidget.h"
#include "Components/ProgressBar.h"

void UMFPSHealthBarWidget::SetHealthPercent(float Percent)
{
	if (!HealthProgressBar) return;

	const auto HealthBarVisibility = (Percent > PercentVisibilityTreshold || FMath::IsNearlyZero(Percent))
		? ESlateVisibility::Hidden : ESlateVisibility::Visible;

	HealthProgressBar->SetVisibility(HealthBarVisibility);

	FLinearColor HealthBarColor;

	if (Percent > PercentGoodColorTreshold)
	{
		HealthBarColor = GoodColor;
	}
	else if (Percent > PercentBadColorTreshold)
	{
		HealthBarColor = MiddleColor;
	}
	else
		HealthBarColor = BadColor;

	HealthProgressBar->SetFillColorAndOpacity(HealthBarColor);
	HealthProgressBar->SetPercent(Percent);
}
