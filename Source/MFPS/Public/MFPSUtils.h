#pragma once

#include "Components/MFPSHealthComponent.h"
#include "Components/MFPSWeaponComponent.h"
#include "Components/MFPSRespawnComponent.h"

class MFPSUtils
{
public:

	template <typename T>
	static T* GetMFPSPlayerComponent(APawn* PlayerPawn)
	{
		if (!PlayerPawn) return nullptr;

		const auto Component = PlayerPawn->GetComponentByClass(T::StaticClass());
		return Cast<T>(Component);
	}

	template <typename T>
	static T* GetMFPSControllerComponent(AController* Controller)
	{
		if (!Controller) return nullptr;

		const auto Component = Controller->GetComponentByClass(T::StaticClass());
		return Cast<T>(Component);
	}

	static FText TextFromInt(int32 Number) { return FText::FromString(FString::FromInt(Number)); }
};