// MFPS by Oleg Sutugin. All rights reserved


#include "Animations/MFPSAnimNotify.h"

void UMFPSAnimNotify::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	OnNotified.Broadcast(MeshComp);

	Super::Notify(MeshComp, Animation);

}