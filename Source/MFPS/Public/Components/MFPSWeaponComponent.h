// MFPS by Oleg Sutugin. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "MFPSTypes.h"
#include "MFPSWeaponComponent.generated.h"

class AMFPSBaseWeapon;
class AMFPSDummyWeapon;

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class MFPS_API UMFPSWeaponComponent : public UActorComponent
{
	GENERATED_BODY()

public:

	UMFPSWeaponComponent();

protected:
	//classes for weap and dummy
	UPROPERTY(Replicated, EditDefaultsOnly, Category = "Weapon")
		TArray<FWeaponData> WeaponData;

	UPROPERTY(Replicated, EditDefaultsOnly, Category = "Weapon")
		TArray<FDummyWeaponData> DummyWeaponData;

	//weaponssockets for weap and dummy
	UPROPERTY(Replicated, EditDefaultsOnly, Category = "Weapon")
		FName WeaponEquipSocketName = "WeaponSocket";

	UPROPERTY(Replicated, EditDefaultsOnly, Category = "Weapon")
		FName DummyWeaponEquipSocketName = "DummyWeaponSocket";

	UPROPERTY(Replicated, EditDefaultsOnly, Category = "Weapon")
		FName WeaponArmorySocketName = "ArmorySocket";

	UPROPERTY(Replicated, EditDefaultsOnly, Category = "Weapon")
		FName DummyWeaponArmorySocketName = "DummyArmorySocket";

	//animations
	UPROPERTY(EditDefaultsOnly, Category = "Animation")
		UAnimMontage* EquipAnimMontageFPS;

	UPROPERTY(EditDefaultsOnly, Category = "Animation")
		UAnimMontage* EquipAnimMontageTPS;

	UPROPERTY(EditDefaultsOnly, Category = "Animation")
		UAnimMontage* FireAnimMontageFPS;

	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:
	UFUNCTION(Server, Reliable)
		void StartFire();

	UFUNCTION(Server, Reliable)
		void StopFire();
	UFUNCTION(Server, Reliable)
		void S_NextWeapon();

	UFUNCTION(Server, Reliable)
		void Reload();


	bool GetWeaponUIData(FWeaponUIData& UIData);
	bool GetWeaponAmmoData(FAmmoData& AmmoData);
	bool TryToAddAmmo(TSubclassOf<AMFPSBaseWeapon> WeaponType, int32 ClipsValue);

	UPROPERTY(Replicated, EditDefaultsOnly, BlueprintReadWrite)
		AMFPSBaseWeapon* CurrentWeapon = nullptr;
	UPROPERTY(Replicated)
		AMFPSDummyWeapon* CurrentDummyWeapon = nullptr;
private:


	UPROPERTY(Replicated)
		TArray<AMFPSBaseWeapon*> Weapons;

	UPROPERTY(Replicated)
		TArray<AMFPSDummyWeapon*> DummyWeapons;

	//new
	UPROPERTY()
		UAnimMontage* CurrentReloadAnimMontage = nullptr;
	UPROPERTY()
		UAnimMontage* CurrentDummyReloadAnimMontage = nullptr;

	UPROPERTY(Replicated)
		int32 CurrentWeaponIndex = 0;

	bool EquipAnimInProgress = false;
	bool ReloadAnimInProgress = false;


	void C_SpawnWeapons();
	void S_AttachWeaponToSocket(AActor* Weapon, USceneComponent* Mesh, const FName& SocketName);
	void S_EquipWeapon(int32 WeaponIndex);
	void PlayAnimMontageFPS(UAnimMontage* Animation);

	UFUNCTION(Server, Unreliable)
		void PlayAnimMontageTPS_OnServer(UAnimMontage* Animation);

	void InitAnimations();
	void OnEquipFinished(USkeletalMeshComponent* MeshComponent);
	void OnReloadFinished(USkeletalMeshComponent* MeshComponent);

	
	bool CanFire() const;
	bool CanEquip() const;
	bool CanReload() const;

	void OnEmptyClip(AMFPSBaseWeapon* AmmoEmptyWeapon);

	void ChangeClip();
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
};
