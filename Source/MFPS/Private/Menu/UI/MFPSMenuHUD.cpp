// MFPS by Oleg Sutugin. All rights reserved


#include "Menu/UI/MFPSMenuHUD.h"
#include "Menu/UI/MFPSMenuWidget.h"

void AMFPSMenuHUD::BeginPlay()
{
	Super::BeginPlay();

	if (MenuWidgetClass)
	{
		const auto MenuWidget = CreateWidget<UUserWidget>(GetWorld(), MenuWidgetClass);
		if (MenuWidget)
		{
			MenuWidget->AddToViewport();
		}
	}
}
