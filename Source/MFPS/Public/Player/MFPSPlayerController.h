// MFPS by Oleg Sutugin. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "MFPSTypes.h"
#include "GameFramework/PlayerController.h"
#include "MFPSPlayerController.generated.h"

class UMFPSRespawnComponent;

UCLASS()
class MFPS_API AMFPSPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	AMFPSPlayerController();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "RespawnComponent")
		UMFPSRespawnComponent* RespawnComponent;

	UFUNCTION(Server,Reliable)
	virtual void OnPossess(APawn* InPawn) override;

	UFUNCTION(NetMulticast,Reliable)
	void M_OnPossess(APawn* InPawn);

	virtual void BeginPlay() override;
	virtual void SetupInputComponent() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bIsPaused = false;

	UFUNCTION(BlueprintCallable)
	void OnPauseGame();

	
private:

	void OnMatchStateChanged(EMFPSMatchState State);
};
