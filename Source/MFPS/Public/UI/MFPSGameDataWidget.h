// MFPS by Oleg Sutugin. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MFPSGameDataWidget.generated.h"


UCLASS()
class MFPS_API UMFPSGameDataWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable, Category = "UI")
		int32 GetRoundTimeRemaining() const;

	UFUNCTION(BlueprintCallable, Category = "UI")
		int32 GetPlayerKills() const;

	UFUNCTION(BlueprintCallable, Category = "UI")
		int32 GetPlayerDeaths() const;
};
