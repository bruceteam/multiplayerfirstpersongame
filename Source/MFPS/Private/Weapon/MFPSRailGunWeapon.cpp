// MFPS by Oleg Sutugin. All rights reserved


#include "Weapon/MFPSRailGunWeapon.h"
#include "Engine/DamageEvents.h"
#include "NiagaraFunctionLibrary.h"
#include "Components/MFPSWeaponFXComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"
#include "NiagaraComponent.h"

AMFPSRailGunWeapon::AMFPSRailGunWeapon()
{
	WeaponFXComponent = CreateDefaultSubobject<UMFPSWeaponFXComponent>("WeaponFXComponent");
}

void AMFPSRailGunWeapon::StartFire()
{
	S_MakeShot();
}


void AMFPSRailGunWeapon::S_MakeShot_Implementation()
{
	if (HasAuthority())
	{
		if (!GetWorld()) return;

		if (IsAmmoEmpty())
		{
			UGameplayStatics::SpawnSoundAtLocation(GetWorld(), NoAmmoSound, GetActorLocation());
			return;
		}

		M_MakeShot();
	}
}

void AMFPSRailGunWeapon::M_MakeShot_Implementation()
{
	FVector TraceStart, TraceEnd;
	if (!GetTraceData(TraceStart, TraceEnd)) return;

	FHitResult HitResult;
	MakeHit(HitResult, TraceStart, TraceEnd);

	FVector TraceFXEnd = TraceEnd;
	if (HitResult.bBlockingHit)
	{
		TraceFXEnd = HitResult.ImpactPoint;
		MakeDamage(HitResult);
		WeaponFXComponent->S_PlayImpactFX(HitResult);
	}
	S_SpawnTraceFX(GetMuzzleWorldLocation(), TraceFXEnd);
	DecreaseAmmo();
	SpawnMuzzleFX();
	UGameplayStatics::SpawnSoundAttached(FireSound, WeaponMesh, MuzzleSocketName);
}

void AMFPSRailGunWeapon::BeginPlay()
{
	Super::BeginPlay();
	check(WeaponFXComponent);
}

void AMFPSRailGunWeapon::MakeDamage(const FHitResult& HitResult)
{
	const auto DamagedActor = HitResult.GetActor();
	if (!DamagedActor) return;

	DamagedActor->TakeDamage(DamageAmount, FDamageEvent(),
		GetController(), this);
}

void AMFPSRailGunWeapon::S_SpawnTraceFX_Implementation(const FVector& TraceStart, const FVector& TraceEnd)
{
	M_SpawnTraceFX(TraceStart, TraceEnd);
}

void AMFPSRailGunWeapon::M_SpawnTraceFX_Implementation(const FVector& TraceStart, const FVector& TraceEnd)
{
	const auto TraceFXComponent = UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), TraceFX, TraceStart);

	if (TraceFXComponent)
	{
		TraceFXComponent->SetNiagaraVariableVec3(TraceTargetName, TraceEnd);
	}
}

AController* AMFPSRailGunWeapon::GetController() const
{
	const auto Pawn = Cast<APawn>(GetOwner());
	return Pawn ? Pawn->GetController() : nullptr;
}