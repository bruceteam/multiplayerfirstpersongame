// MFPS by Oleg Sutugin. All rights reserved


#include "Components/MFPSCharacterMovementComponent.h"
#include "Player/MFPSCharacter.h"

float UMFPSCharacterMovementComponent::GetMaxSpeed() const
{
	const float MaxSpeed = Super::GetMaxSpeed();
	const AMFPSCharacter* Player = Cast<AMFPSCharacter>(GetPawnOwner());

	return MaxSpeed;
}
