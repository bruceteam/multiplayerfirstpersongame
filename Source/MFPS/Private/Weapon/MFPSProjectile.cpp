// MFPS by Oleg Sutugin. All rights reserved


#include "Weapon/MFPSProjectile.h"
#include "Components/SphereComponent.h"
#include "GameFrameWork/ProjectileMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Components/MFPSWeaponFXComponent.h"

// Sets default values
AMFPSProjectile::AMFPSProjectile()
{
	PrimaryActorTick.bCanEverTick = false;

	CollisionComponent = CreateDefaultSubobject<USphereComponent>("CollisionComponent");
	CollisionComponent->InitSphereRadius(3.0f);
	CollisionComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	CollisionComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
	CollisionComponent->bReturnMaterialOnMove = true;
	SetRootComponent(CollisionComponent);

	MovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>("MovementComponent");
	MovementComponent->InitialSpeed = 3000.0f;
	MovementComponent->ProjectileGravityScale = 0.05f;

	WeaponFXComponent = CreateDefaultSubobject<UMFPSWeaponFXComponent>("WeaponFXComponent");
}


void AMFPSProjectile::BeginPlay()
{
	Super::BeginPlay();
	
	check(MovementComponent);
	check(CollisionComponent);
	check(WeaponFXComponent);

	MovementComponent->Velocity = ShotDirection * MovementComponent->InitialSpeed;
	CollisionComponent->IgnoreActorWhenMoving(GetOwner(), true);
	CollisionComponent->OnComponentHit.AddDynamic(this, &AMFPSProjectile::OnProjectileHit);
	SetLifeSpan(5.0f);
}

void AMFPSProjectile::OnProjectileHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, 
	UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (!GetWorld()) return;

	MovementComponent->StopMovementImmediately();

	UGameplayStatics::ApplyPointDamage(OtherActor, DamageByHit, Hit.TraceStart,
		Hit, GetController(), GetOwner(), UDamageType::StaticClass());

	WeaponFXComponent->S_PlayImpactFX(Hit);
	Destroy();
}

AController* AMFPSProjectile::GetController() const
{
	const auto Pawn = Cast<APawn>(GetOwner());
	return Pawn ? Pawn->GetController() : nullptr;
}


