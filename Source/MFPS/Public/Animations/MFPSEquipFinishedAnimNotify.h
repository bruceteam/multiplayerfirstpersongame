// MFPS by Oleg Sutugin. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "MFPSAnimNotify.h" 
#include "MFPSEquipFinishedAnimNotify.generated.h"


UCLASS()
class MFPS_API UMFPSEquipFinishedAnimNotify : public UMFPSAnimNotify
{
	GENERATED_BODY()
	
};
