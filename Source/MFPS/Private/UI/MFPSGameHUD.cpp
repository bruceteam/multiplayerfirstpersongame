// MFPS by Oleg Sutugin. All rights reserved


#include "UI/MFPSGameHUD.h"
#include "MFPS/MFPSGameModeBase.h"
#include "Player/MFPSPlayerController.h"
#include "MFPSGameStateBase.h"
#include "Blueprint/UserWidget.h"

DEFINE_LOG_CATEGORY_STATIC(LogMFPSGameHUD, All, All);

void AMFPSGameHUD::BeginPlay()
{
	Super::BeginPlay();



	GameWidgets.Add(EMFPSMatchState::InProgress, CreateWidget(GetWorld(), PlayerHUDWidgetClass));
	GameWidgets.Add(EMFPSMatchState::GameOver, CreateWidget(GetWorld(), GameOverWidgetClass));

	for (auto GameWidgetPair : GameWidgets)
	{
		const auto GameWidget = GameWidgetPair.Value;
		if (!GameWidget) continue;

		GameWidget->AddToViewport();
		GameWidget->SetVisibility(ESlateVisibility::Hidden);
	}
	
	auto PauseWidget = CreateWidget<UUserWidget>(GetWorld(), PauseWidgetClass);
	if(PauseWidget)
	{
		PauseWidget->AddToViewport();
	}

	if (GetWorld())
	{
		const auto GS = Cast<AMFPSGameStateBase>(GetWorld()->GetGameState());
		if (GS)
		{
			GS->OnMatchStateChanged.AddUObject(this, &AMFPSGameHUD::OnMatchStateChanged);
		}
	}
}

void AMFPSGameHUD::OnMatchStateChanged(EMFPSMatchState State)
{
	if (CurrentWidget)
	{
		CurrentWidget->SetVisibility(ESlateVisibility::Hidden);
	}

	if (GameWidgets.Contains(State))
	{
		CurrentWidget = GameWidgets[State];
	}

	if (CurrentWidget)
	{
		CurrentWidget->SetVisibility(ESlateVisibility::Visible);
	}

	UE_LOG(LogMFPSGameHUD, Display, TEXT("Match State CHanged : %s"), *UEnum::GetValueAsString(State));
}
