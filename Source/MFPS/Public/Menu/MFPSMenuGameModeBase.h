// MFPS by Oleg Sutugin. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MFPSMenuGameModeBase.generated.h"


UCLASS()
class MFPS_API AMFPSMenuGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	AMFPSMenuGameModeBase();
};
