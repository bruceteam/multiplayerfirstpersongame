// MFPS by Oleg Sutugin. All rights reserved


#include "Components/MFPSWeaponComponent.h"
#include "Weapon/MFPSBaseWeapon.h"
#include "Weapon/MFPSDummyWeapon.h"
#include "Animations/MFPSEquipFinishedAnimNotify.h"
#include "Animations/MFPSReloadFinishedAnimNotify.h"
#include "Animations/AnimUtils.h"
#include "Player/MFPSCharacter.h"
#include "Net/UnrealNetwork.h"

DEFINE_LOG_CATEGORY_STATIC(LogWeaponComponent, All, All);

UMFPSWeaponComponent::UMFPSWeaponComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}


void UMFPSWeaponComponent::StartFire_Implementation()
{
	if (!CanFire()) return;

	if(!CurrentWeapon->GetAmmoData().Bullets == 1 && CurrentDummyWeapon->bIsInfiniteEffect)
	{
		UE_LOG(LogWeaponComponent, Display, TEXT("1 Bull"));
	}
	else CurrentDummyWeapon->StartFire();
	CurrentWeapon->StartFire();

}

void UMFPSWeaponComponent::StopFire_Implementation()
{
	if (!CurrentWeapon) return;

	CurrentWeapon->StopFire();
	CurrentDummyWeapon->StopFire();
}

void UMFPSWeaponComponent::BeginPlay()
{
	Super::BeginPlay();

	CurrentWeaponIndex = 0;
	InitAnimations();
	C_SpawnWeapons();
	S_EquipWeapon(CurrentWeaponIndex);
}

void UMFPSWeaponComponent::Reload_Implementation()
{
	ChangeClip();
}

void UMFPSWeaponComponent::C_SpawnWeapons()
{
	if (GetOwner()->HasAuthority())
	{
		const AMFPSCharacter* Character = Cast<AMFPSCharacter>(GetOwner());
		if (!Character || !GetWorld()) return;

		for (auto OneWeaponData : WeaponData)
		{
			auto Weapon = GetWorld()->SpawnActor<AMFPSBaseWeapon>(OneWeaponData.WeaponClass);
			if (!Weapon) continue;

			Weapon->OnEmptyClip.AddUObject(this, &UMFPSWeaponComponent::OnEmptyClip);
			Weapon->SetOwner(GetOwner());
			Weapons.Add(Weapon);

			S_AttachWeaponToSocket(Weapon, Character->SkeletalMeshFPS, WeaponArmorySocketName);
		}

		for (auto OneDummyWeaponData : DummyWeaponData)
		{
			auto DummyWeapon = GetWorld()->SpawnActor<AMFPSDummyWeapon>(OneDummyWeaponData.DummyWeaponClass);
			if (!DummyWeapon) continue;

			DummyWeapon->SetOwner(GetOwner());
			DummyWeapons.Add(DummyWeapon);

			S_AttachWeaponToSocket(DummyWeapon, Character->SkeletalMeshTPS, DummyWeaponArmorySocketName);
		}
	}
}


void UMFPSWeaponComponent::S_AttachWeaponToSocket(AActor* Weapon, USceneComponent* Mesh, const FName& SocketName)
{
	if (!Weapon || !Mesh) return;

	FAttachmentTransformRules AttachmentRules(EAttachmentRule::SnapToTarget, false);
	Weapon->AttachToComponent(Mesh, AttachmentRules, SocketName);
}

void UMFPSWeaponComponent::S_EquipWeapon(int32 WeaponIndex)
{
	if (GetOwner()->HasAuthority())
	{
		UE_LOG(LogTemp, Warning, TEXT("Weapon Index = %i, Weapons array: %i, DummyWeapons array :%i"), WeaponIndex, Weapons.Num(), DummyWeapons.Num());

		if (WeaponIndex < 0 || WeaponIndex >= Weapons.Num())
		{
			UE_LOG(LogWeaponComponent, Display, TEXT("InvalidWeapon index!"));
			return;
		}

		const AMFPSCharacter* Character = Cast<AMFPSCharacter>(GetOwner());
		if (!Character) return;

		if (CurrentWeapon)
		{
			CurrentWeapon->StopFire();
			S_AttachWeaponToSocket(CurrentWeapon, Character->SkeletalMeshFPS, WeaponArmorySocketName);
		}

		CurrentWeapon = Weapons[WeaponIndex];

		//predicate + lambda
		const auto CurrentWeaponData = WeaponData.FindByPredicate([&](const FWeaponData& Data) {
			return Data.WeaponClass == CurrentWeapon->GetClass(); });
		CurrentReloadAnimMontage = CurrentWeaponData ? CurrentWeaponData->ReloadWeaponAnimMontage : nullptr;

		S_AttachWeaponToSocket(CurrentWeapon, Character->SkeletalMeshFPS, WeaponEquipSocketName);

		if (CurrentDummyWeapon)
		{
			CurrentDummyWeapon->StopFire();
			S_AttachWeaponToSocket(CurrentDummyWeapon, Character->SkeletalMeshTPS, DummyWeaponArmorySocketName);
		}

		CurrentDummyWeapon = DummyWeapons[WeaponIndex];

		const auto CurrentDummyWeaponData = DummyWeaponData.FindByPredicate([&](const FDummyWeaponData& Data) {
			return Data.DummyWeaponClass == CurrentDummyWeapon->GetClass(); });
		CurrentDummyReloadAnimMontage = CurrentDummyWeaponData ? CurrentDummyWeaponData->ReloadDummyAnimMontage : nullptr;

		S_AttachWeaponToSocket(CurrentDummyWeapon, Character->SkeletalMeshTPS, DummyWeaponEquipSocketName);
		EquipAnimInProgress = true;

		PlayAnimMontageFPS(EquipAnimMontageFPS);
		PlayAnimMontageTPS_OnServer(EquipAnimMontageTPS);
	}
}

void UMFPSWeaponComponent::S_NextWeapon_Implementation()
{
	if (GetOwner()->HasAuthority())
	{
		if (!CanEquip()) return;

		CurrentWeaponIndex = (CurrentWeaponIndex + 1) % Weapons.Num();
		S_EquipWeapon(CurrentWeaponIndex);
	}
}

void UMFPSWeaponComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	CurrentWeapon = nullptr;
	for (auto Weapon : Weapons)
	{
		Weapon->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		Weapon->Destroy();
	}
	Weapons.Empty();

	CurrentDummyWeapon = nullptr;
	for (auto DummyWeapon : DummyWeapons)
	{
		DummyWeapon->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		DummyWeapon->Destroy();
	}
	DummyWeapons.Empty();

	Super::EndPlay(EndPlayReason);
}

void UMFPSWeaponComponent::PlayAnimMontageFPS(UAnimMontage* Animation)
{
	AMFPSCharacter* Character = Cast<AMFPSCharacter>(GetOwner());
	if (!Character) return;

	Character->PlayAMontage_Multicast(Animation, Character->SkeletalMeshFPS);
}

void UMFPSWeaponComponent::PlayAnimMontageTPS_OnServer_Implementation(UAnimMontage* Animation)
{
	AMFPSCharacter* Character = Cast<AMFPSCharacter>(GetOwner());
	if (!Character) return;

	CurrentDummyWeapon->StopFire();
	Character->PlayAMontage_Multicast(Animation, Character->SkeletalMeshTPS);
}

void UMFPSWeaponComponent::InitAnimations()
{
	auto EquipFinishNotify = AnimUtils::FindNotifyByClass<UMFPSEquipFinishedAnimNotify>(EquipAnimMontageFPS);
	if (EquipFinishNotify)
	{
		EquipFinishNotify->OnNotified.AddUObject(this, &UMFPSWeaponComponent::OnEquipFinished);
	}
	else
	{
		UE_LOG(LogWeaponComponent, Error, TEXT("You are forgot to set anim notify!"));
	}

	for (auto OneWeaponData : WeaponData)
	{
		auto ReloadFinishNotify = AnimUtils::FindNotifyByClass<UMFPSReloadFinishedAnimNotify>(OneWeaponData.ReloadWeaponAnimMontage);
		if (!ReloadFinishNotify) continue;

		ReloadFinishNotify->OnNotified.AddUObject(this, &UMFPSWeaponComponent::OnReloadFinished);
	}
}

void UMFPSWeaponComponent::OnEquipFinished(USkeletalMeshComponent* MeshComponent)
{
	AMFPSCharacter* Character = Cast<AMFPSCharacter>(GetOwner());
	if (!Character || Character->SkeletalMeshFPS != MeshComponent) return;

	EquipAnimInProgress = false;
}

void UMFPSWeaponComponent::OnReloadFinished(USkeletalMeshComponent* MeshComponent)
{
	AMFPSCharacter* Character = Cast<AMFPSCharacter>(GetOwner());
	if (!Character || Character->SkeletalMeshFPS != MeshComponent) return;

	ReloadAnimInProgress = false;
}


bool UMFPSWeaponComponent::CanFire() const
{
	return CurrentWeapon && !EquipAnimInProgress && !ReloadAnimInProgress && CurrentWeapon->GetAmmoData().Bullets != 0;
}

bool UMFPSWeaponComponent::CanEquip() const
{
	return !EquipAnimInProgress && !ReloadAnimInProgress;
}

bool UMFPSWeaponComponent::CanReload() const
{
	return CurrentWeapon && !EquipAnimInProgress && !ReloadAnimInProgress && CurrentWeapon->CanReload();
}

void UMFPSWeaponComponent::OnEmptyClip(AMFPSBaseWeapon* AmmoEmptyWeapon)
{
	if (!AmmoEmptyWeapon) return;

	if (CurrentWeapon == AmmoEmptyWeapon)
	{
		CurrentDummyWeapon->StopFire();
		ChangeClip();
	}
	else
	{
		for (const auto Weapon : Weapons)
		{
			if (Weapon == AmmoEmptyWeapon)
			{
				Weapon->ChangeClip();
			}
		}
	}
}

void UMFPSWeaponComponent::ChangeClip()
{
	if (!CanReload()) return;
	CurrentWeapon->StopFire();
	CurrentDummyWeapon->StopFire();
	CurrentWeapon->ChangeClip();

	ReloadAnimInProgress = true;
	PlayAnimMontageFPS(CurrentReloadAnimMontage);
	PlayAnimMontageTPS_OnServer(CurrentDummyReloadAnimMontage);
}

bool UMFPSWeaponComponent::GetWeaponUIData(FWeaponUIData& UIData)
{
	if (CurrentWeapon)
	{
		UIData = CurrentWeapon->GetWeaponUIData();
		return true;
	}
	return false;
}

bool UMFPSWeaponComponent::GetWeaponAmmoData(FAmmoData& AmmoData)
{
	if (CurrentWeapon)
	{
		AmmoData = CurrentWeapon->GetAmmoData();
		return true;
	}
	return false;
}

bool UMFPSWeaponComponent::TryToAddAmmo(TSubclassOf<AMFPSBaseWeapon> WeaponType, int32 ClipsValue)
{
	for (const auto Weapon : Weapons)
	{
		if (Weapon && Weapon->IsA(WeaponType))
		{
			return Weapon->TryToAddAmmo(ClipsValue);
		}
	}
	return false;
}

void UMFPSWeaponComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const   //here
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Here we list the variables we want to replicate
	DOREPLIFETIME(UMFPSWeaponComponent, CurrentWeapon);
	DOREPLIFETIME(UMFPSWeaponComponent, CurrentDummyWeapon);
	DOREPLIFETIME(UMFPSWeaponComponent, Weapons);
	DOREPLIFETIME(UMFPSWeaponComponent, DummyWeapons);
	DOREPLIFETIME(UMFPSWeaponComponent, WeaponData);
	DOREPLIFETIME(UMFPSWeaponComponent, DummyWeaponData);

	DOREPLIFETIME(UMFPSWeaponComponent, WeaponEquipSocketName);
	DOREPLIFETIME(UMFPSWeaponComponent, DummyWeaponEquipSocketName);
	DOREPLIFETIME(UMFPSWeaponComponent, WeaponArmorySocketName);
	DOREPLIFETIME(UMFPSWeaponComponent, DummyWeaponArmorySocketName);

	DOREPLIFETIME(UMFPSWeaponComponent, CurrentWeaponIndex);
}