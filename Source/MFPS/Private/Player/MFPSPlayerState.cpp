// MFPS by Oleg Sutugin. All rights reserved


#include "Player/MFPSPlayerState.h"

DEFINE_LOG_CATEGORY_STATIC(LogMFPSGameState, All, All);


void AMFPSPlayerState::LogInfo() 
{
	UE_LOG(LogMFPSGameState, Display, TEXT("TeamID %i, Kills %i, Deaths %i"),TeamID, KillsNum, DeathNum);
}

void AMFPSPlayerState::AddKill_Implementation()
{
	++KillsNum; 
}

void AMFPSPlayerState::AddDeath_Implementation()
{
	++DeathNum; 
}
