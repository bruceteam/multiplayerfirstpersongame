// MFPS by Oleg Sutugin. All rights reserved


#include "Player/MFPSCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/MFPSHealthComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/MFPSCharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"
#include "Components/MFPSWeaponComponent.h"
#include "Components/InputComponent.h"
#include "Net/UnrealNetwork.h"
#include "GameFramework/Controller.h"
#include "Components/WidgetComponent.h"
#include "UI/MFPSHealthBarWidget.h"
#include "MFPS/MFPSGameModeBase.h"
#include "Components/ProgressBar.h"
#include "Components/CapsuleComponent.h"



AMFPSCharacter::AMFPSCharacter(const FObjectInitializer& ObjInit) ://
	Super(ObjInit.SetDefaultSubobjectClass<UMFPSCharacterMovementComponent>(ACharacter::CharacterMovementComponentName))
{
	PrimaryActorTick.bCanEverTick = true;

	bReplicates = true;
	SetReplicateMovement(true);

	CameraComponent = CreateDefaultSubobject<UCameraComponent>("CameraComponent");
	CameraComponent->SetupAttachment(GetRootComponent());
	CameraComponent->bUsePawnControlRotation = true;

	SkeletalMeshFPS = CreateDefaultSubobject<USkeletalMeshComponent>("SceletalMeshFPS");
	SkeletalMeshFPS->SetupAttachment(CameraComponent);

	SkeletalMeshTPS = CreateDefaultSubobject<USkeletalMeshComponent>("SceletalMeshTPS");
	SkeletalMeshTPS->SetupAttachment(GetRootComponent());

	HealthComponent = CreateDefaultSubobject<UMFPSHealthComponent>("HealthComponent");
	WeaponComponent = CreateDefaultSubobject<UMFPSWeaponComponent>("WeaponComponent");

	HealthWidgetComponent = CreateDefaultSubobject<UWidgetComponent>("HealthBarWidget");
	HealthWidgetComponent->SetupAttachment(GetRootComponent());
	HealthWidgetComponent->SetWidgetSpace(EWidgetSpace::Screen);
	HealthWidgetComponent->SetOwnerNoSee(true);
}

float AMFPSCharacter::GetMovementDirection() const
{
	if (GetVelocity().IsZero()) return 0.0f;

	const auto VelocityNormal = GetVelocity().GetSafeNormal();
	const auto AngleBetween = FMath::Acos(FVector::DotProduct(GetActorForwardVector(), VelocityNormal));
	const auto CrossProduct = FVector::CrossProduct(GetActorForwardVector(), VelocityNormal);

	const auto Degrees = FMath::RadiansToDegrees(AngleBetween);

	return CrossProduct.IsZero() ? Degrees : Degrees * FMath::Sign(CrossProduct.Z);
}

void AMFPSCharacter::BeginPlay()
{
	Super::BeginPlay();

	HealthComponent->OnDeath.AddUObject(this, &AMFPSCharacter::OnDeath);
	HealthComponent->OnHealthChanged.AddUObject(this, &AMFPSCharacter::OnHealthChanged);

	const auto GM = Cast<AMFPSGameModeBase>(GetWorld()->GetAuthGameMode());
	if (GM) GM->OnGameOver.AddUObject(this, &AMFPSCharacter::GameOver);

	const auto HealthBarWidget = Cast<UMFPSHealthBarWidget>(HealthWidgetComponent->GetUserWidgetObject());
	if (HealthBarWidget)
	{
		HealthBarWidget->HealthProgressBar->SetVisibility(ESlateVisibility::Hidden);
	}
}

void AMFPSCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AMFPSCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &AMFPSCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AMFPSCharacter::MoveRight);
	PlayerInputComponent->BindAxis("LookUP", this, &AMFPSCharacter::LookUP);
	PlayerInputComponent->BindAxis("Turn", this, &AMFPSCharacter::Turn);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AMFPSCharacter::Jump);
	PlayerInputComponent->BindAction("Fire", IE_Pressed, WeaponComponent, &UMFPSWeaponComponent::StartFire);
	PlayerInputComponent->BindAction("Fire", IE_Released, WeaponComponent, &UMFPSWeaponComponent::StopFire);
	PlayerInputComponent->BindAction("NextWeapon", IE_Pressed, WeaponComponent, &UMFPSWeaponComponent::S_NextWeapon);
	PlayerInputComponent->BindAction("Reload", IE_Pressed, WeaponComponent, &UMFPSWeaponComponent::Reload);
}

void AMFPSCharacter::MoveForward(float Value)
{
	if (Value == 0.0f) return;
	AddMovementInput(GetActorForwardVector(), Value);
}

void AMFPSCharacter::MoveRight(float Value)
{
	if (Value == 0.0f) return;
	AddMovementInput(GetActorRightVector(), Value);
}

void AMFPSCharacter::LookUP(float Value)
{
	AddControllerPitchInput(Value);

	if (IsLocallyControlled())
	{
		FRotator Rotation = GetControlRotation();
		if (HasAuthority())
		{
			M_LookUPSync(Rotation);
		}
		else
		{
			S_LookUPSync(Rotation);
		}
	}
}

void AMFPSCharacter::S_LookUPSync_Implementation(FRotator RotationSync)
{
	ControlRotationSynchronized = RotationSync;
	if (!IsLocallyControlled())
	{
		CameraComponent->SetWorldRotation(ControlRotationSynchronized);
	}
}

void AMFPSCharacter::M_LookUPSync_Implementation(FRotator RotationSync)
{
	ControlRotationSynchronized = RotationSync;
	if (!IsLocallyControlled())
	{
		CameraComponent->SetWorldRotation(ControlRotationSynchronized);
	}
}

void AMFPSCharacter::Turn(float Value)
{
	AddControllerYawInput(Value);
}

void AMFPSCharacter::OnDeath()
{

	UE_LOG(LogTemp, Warning, TEXT("Player is Dead!!"));

	GetCharacterMovement()->DisableMovement();
	SetLifeSpan(LifeSpanOnDeath);

	if (Controller)
	{
		Controller->ChangeState(NAME_Spectating);
	}
	GetCapsuleComponent()->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	SkeletalMeshTPS->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
	WeaponComponent->StopFire();
	SkeletalMeshTPS->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
	SkeletalMeshTPS->SetSimulatePhysics(true);

	S_PlayDeathSound();
}

void AMFPSCharacter::S_PlayDeathSound_Implementation()
{
	M_PlayDeathSound();
}

void AMFPSCharacter::M_PlayDeathSound_Implementation()
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), DeathSound, GetActorLocation());
}

void AMFPSCharacter::OnHealthChanged(float Health, bool bIsDamage)
{
	const auto HealthBarWidget = Cast<UMFPSHealthBarWidget>(HealthWidgetComponent->GetUserWidgetObject());
	if (!HealthBarWidget) return;

	HealthBarWidget->SetHealthPercent(HealthComponent->GetHealthPercent());
}

///test actually work!
void AMFPSCharacter::PlayAMontage_Multicast_Implementation(class UAnimMontage* AnimMontage, USkeletalMeshComponent* MeshComponent, float InPlayRate, FName StartSectionName)
{
	UAnimInstance* AnimInstance = (MeshComponent) ? MeshComponent->GetAnimInstance() : nullptr;


	if (AnimMontage && AnimInstance)
	{
		float const Duration = AnimInstance->Montage_Play(AnimMontage, InPlayRate);

		if (Duration > 0.f)
		{
			// Start at a given Section.
			if (StartSectionName != NAME_None)
			{
				AnimInstance->Montage_JumpToSection(StartSectionName, AnimMontage);
			}

			return;
		}
	}

	return;
}

void AMFPSCharacter::SetPlayerColor_Implementation(const FLinearColor& Color)
{
	const auto MaterialInstanceTPS = SkeletalMeshTPS->CreateAndSetMaterialInstanceDynamic(0);
	if (!MaterialInstanceTPS) return;

	MaterialInstanceTPS->SetVectorParameterValue(MaterialColorName, Color);

	const auto MaterialInstanceFPS = SkeletalMeshFPS->CreateAndSetMaterialInstanceDynamic(0);
	if (!MaterialInstanceFPS) return;

	MaterialInstanceFPS->SetVectorParameterValue(MaterialColorName, Color);
}

void AMFPSCharacter::GameOver_Implementation()
{
	this->TurnOff();
	auto PC = Cast<APlayerController>(Controller);
	if (PC) PC->SetInputMode(FInputModeUIOnly());
	
	//this->DisableInput(nullptr);
}

void AMFPSCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const   //here
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Here we list the variables we want to replicate
	DOREPLIFETIME(AMFPSCharacter, WeaponComponent);

}
