// MFPS by Oleg Sutugin. All rights reserved


#include "Menu/MFPSMenuPlayerController.h"

void AMFPSMenuPlayerController::BeginPlay()
{
	Super::BeginPlay();

	SetInputMode(FInputModeUIOnly());
	bShowMouseCursor = true;
}
